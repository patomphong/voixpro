//
//  APISettingsViewController.swift
//  VoixPRO
//
//  Created by Song on 11/24/2559 BE.
//  Copyright © 2559 Song. All rights reserved.
//

import Foundation
import UIKit

class APISettingsViewController: UIViewController {
    
    @IBOutlet var serverApiUrlText: UITextField!
    @IBOutlet var loginSubpath: UITextField!
    
    @IBOutlet var saveAuditSubpath: UITextField!
    @IBOutlet var queryAuditSubpath: UITextField!
    
    @IBOutlet var savePutSubpath: UITextField!
    @IBOutlet var queryPutOrderSubpath: UITextField!
    @IBOutlet var queryPutSubpath: UITextField!
    
    @IBOutlet var savePickSubpath: UITextField!
    @IBOutlet var queryPickOrderSubpath: UITextField!
    @IBOutlet var queryPickSubpath: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fileUtilties.GetApiSettingFromConfigFile()
        
        serverApiUrlText.text = apiHost
        loginSubpath.text = postLogin
        
        saveAuditSubpath.text = postAuditStock
        queryAuditSubpath.text = queryAuditStock
        
        savePutSubpath.text = postPutAway
        queryPutOrderSubpath.text = queryPutAwayOrder
        queryPutSubpath.text = queryPutAway
        savePickSubpath.text = postPick
        queryPickOrderSubpath.text = queryPickOrder
        queryPickSubpath.text = queryPick
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        textField.resignFirstResponder()
        
        return false
    }
    
    @IBAction func backTouchup(_ sender: Any) {
        apiHost = serverApiUrlText.text!
        postLogin = loginSubpath.text!
        postAuditStock = saveAuditSubpath.text!
        queryAuditStock = queryAuditSubpath.text!
        
        postPutAway = savePutSubpath.text!
        queryPutAwayOrder = queryPutOrderSubpath.text!
        queryPutAway = queryPutSubpath.text!
        postPick = savePickSubpath.text!
        queryPickOrder = queryPickOrderSubpath.text!
        queryPick = queryPickSubpath.text!
        
        fileUtilties.SaveApiSettingsToConfigFile()
    }
    
}

