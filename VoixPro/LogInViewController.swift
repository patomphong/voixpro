//
//  LogInViewController.swift
//  VoixPRO
//
//  Created by Song on 10/31/2559 BE.
//  Copyright © 2559 Song. All rights reserved.
//

import Foundation
import UIKit

class LogInViewController : UIViewController, UITextFieldDelegate {
    @IBOutlet var LoginButton: UIButton!
    @IBOutlet var usernameField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var forgotPassword: UIButton!
    @IBOutlet var indicator: UIActivityIndicatorView!
    @IBOutlet var langButton: UIButton!
    @IBOutlet var topLabel: UILabel!
    @IBOutlet var aboutLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Note that SO highlighting makes the new selector syntax (#selector()) look
        // like a comment but it isn't one
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil);
        
        indicator.isHidden = true
        setScreenDisplayAndControlsLocale()
        
        fileUtilties.GetApiSettingFromConfigFile()
        
        #if DEBUG
            usernameField.text = ""
        #endif
        
        aboutLabel.text = "Version \(Bundle.main.releaseVersionNumber!).\(Bundle.main.buildVersionNumber!)"
        
        OperationQueue.main.addOperation {
            self.usernameField.becomeFirstResponder()
        }
        
        //tap to hide keyboard
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard(_:)))
        view.addGestureRecognizer(tap)
        view.isUserInteractionEnabled = true
    }
    
    func hideKeyboard(_ sender: UITapGestureRecognizer) {
        usernameField.resignFirstResponder()
        passwordField.resignFirstResponder()
    }
    
    func keyboardWillShow(sender: NSNotification) {
//        var field: UITextField
        
//        if usernameField.isFirstResponder {
//            field = LoginButton!
//        } else if passwordField.isFirstResponder {
//            field = LoginButton!
//        } else {
//            assert(false, "Not defined input layout handling when keyboard show !!!")
//            return
//        }
        
        if let userInfo = sender.userInfo {
            let kbFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! CGRect)
            let uiFrame = LoginButton.frame
            let uiBottom  = (uiFrame.origin.y + uiFrame.height + 10)
            
            if (kbFrame.origin.y < uiBottom) {
                self.view.frame.origin.y = -(uiBottom - kbFrame.origin.y)
            }
        }
    }
    
    func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func unwindToLoginScreen(segue: UIStoryboardSegue) {
        setScreenDisplayAndControlsLocale()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        switch textField {
        case usernameField:
            passwordField.becomeFirstResponder()
            break
        case passwordField:
            LoginButton.becomeFirstResponder()
            break
        default:
            assert(false, "Not defined input layout handling when keyboard show !!!")
            break
        }
        
        return false
    }
    
    @IBAction func languageTouchup(_ sender: AnyObject) {
        setScreenDisplayAndControlsLocale(true)
    }
    
    func setScreenDisplayAndControlsLocale(_ isSet: Bool = false) {
        Utilities.setScreenDisplayLocale(isSet, langButton, setControlsDisplayTitle: setControlsDisplayTitle)
    }
    
    func setControlsDisplayTitle(_ l: TxtLocaleAbs) {
        topLabel.text = l.loginTitle
        usernameField.placeholder = l.usernamePlaceHolder
        passwordField.placeholder = l.passwordPlaceHolder
        forgotPassword.underlineButton(text: TxtLocale.forgotPasswordTitle)
        LoginButton.setTitle(l.loginButtonTitle, for: UIControlState.normal)
    }
    
    @IBAction func LogInTouchup(_ sender: AnyObject) {
        UIApplication.shared.beginIgnoringInteractionEvents()
        indicator.isHidden = false
        indicator.startAnimating()
        
        let usr = usernameField.text!
        let pwd = passwordField.text!
        
        CheckLogin(usr: usr, pwd: pwd)
    }
    
    func processLoginResult(_ succeeded: Bool, message: String = "") {
        DispatchQueue.main.async {
            self.indicator.isHidden = true
            self.indicator.stopAnimating()
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            UIApplication.shared.endIgnoringInteractionEvents()
            if (succeeded) {
                self.performSegue(withIdentifier: "showMainMenu", sender: self)
            }
            else
            {
                let alertCotroller = Utilities.makeAlert("", msg: message, cancelTitle: "OK")
                self.present(alertCotroller, animated: true, completion: nil)
            }
        }
    }
    
    func CheckLogin(usr: String, pwd: String) {
        
        if usernameField.text!.isEmpty {
            processLoginResult(false, message: TxtLocale.emptyUsername)
            return
        }
        
        let paramString = "Param={\"Username\":\"\(usernameField.text!)\",\"Password\":\"\(passwordField.text!)\"}"
        let postData = NSMutableData(data: paramString.data(using: String.Encoding.utf8)!)
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        if (connectedToNetwork()) {
            let result = postLoginDataToServer(postData)
            if (!result.success) {
                processLoginResult(false, message: result.msg)
            }
        } else {
            processLoginResult(false, message: TxtLocale.noNetworkConnection)
        }
        
    }
    
    func postLoginDataToServer(_ postData: AnyObject) -> (success: Bool, msg: String) {
        func getServerErrMsg (_ m: String) -> String {
            return String(format:TxtLocale.serverResponseFailed, m)
        }
        
        let rest = RestManager(host: apiHost)
        let result = rest.post(postLogin, postJsonData: postData) { (data, response, error) in
            guard error == nil else {
                self.processLoginResult(false, message: String(format: TxtLocale.serverNotReach, postLogin))
                print(error!)
                return
            }
            
            guard let responseData = data else {
                self.processLoginResult(false, message: getServerErrMsg("Error: did not receive data"))
                return
            }
            
            // parse the result as JSON, since that's what the API provides
            do {
                guard let resonseDict = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: Any]
                    else {
                        self.processLoginResult(false, message: getServerErrMsg("Could not get JSON from responseData as dictionary"))
                        return
                }
                
                guard (resonseDict["message"] as? String) == nil else {
                    self.processLoginResult(false, message: resonseDict["message"] as! String)
                    return
                }
                
                guard let authen = resonseDict["authen"] as? Bool else {
                    self.processLoginResult(false, message: getServerErrMsg("Could not parse response data1"))
                    return
                }
                
                var msg = ""
                
                if (authen) {
                    guard let dataTag = resonseDict["Data"] as? [String: Any] else {
                        self.processLoginResult(false, message: getServerErrMsg("Could not parse response data2"))
                        return
                    }
                    guard let fname = dataTag["Firstname"] as? String else {
                        self.processLoginResult(false, message: getServerErrMsg("Could not parse response data3"))
                        return
                    }
                    guard let lname = dataTag["Lastname"] as? String else {
                        self.processLoginResult(false, message: getServerErrMsg("Could not parse response data4"))
                        return
                    }
                    guard let email = dataTag["Email"] as? String else {
                        self.processLoginResult(false, message: getServerErrMsg("Could not parse response data5"))
                        return
                    }
                    
                    UserInfos.shelfNo = dataTag["ShelfNo"] as? String
                    UserInfos.firstName = fname
                    UserInfos.lastName = lname
                    UserInfos.email = email
                    UserInfos.userName = self.usernameField.text!
                }
                else
                {
                    guard let resData = resonseDict["ResponseData"] as? [String: AnyObject] else {
                        self.processLoginResult(false, message: getServerErrMsg("Could not parse response data6"))
                        return
                    }
                    
                    guard let messages = resData["Message"] as? [String: AnyObject] else {
                        self.processLoginResult(false, message: getServerErrMsg("Could not parse response data7"))
                        return
                    }
                    
                    guard let message = messages[TxtLocale.symbol] as? String else {
                        self.processLoginResult(false, message: getServerErrMsg("Could not parse response data8"))
                        return
                    }
                    
                    UserInfos.clear()
                    msg = message
                }
                
                self.processLoginResult(authen, message: msg)
                
            } catch  {
                self.processLoginResult(false, message: getServerErrMsg("error parsing response from POST on \(postLogin)"))
                return
            }
        }
        
        return result
    }
    
}
