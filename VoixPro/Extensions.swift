//
//  extensions.swift
//  VoixPRO
//
//  Created by Song on 12/10/2559 BE.
//  Copyright © 2559 Song. All rights reserved.
//

import Foundation
import UIKit

public extension Double {
    private static var numberFormatter: NumberFormatter? = nil
    public func toCurrecyDigits(numOfPlace: Int, _ removeZeroPlace: Bool = false) -> String {
        if Double.numberFormatter == nil {
            Double.numberFormatter = NumberFormatter()
            Double.numberFormatter!.numberStyle = NumberFormatter.Style.decimal
            Double.numberFormatter!.minimumFractionDigits = 7
            Double.numberFormatter!.maximumFractionDigits = 7
        }
        
        var s = Double.numberFormatter!.string(from: NSNumber(value: self))!
        let idx = s.index(s.endIndex, offsetBy: -(3 + numOfPlace))
        s = s.substring(to: idx)
        if removeZeroPlace {
            let zeroPlaceString = String(repeating: "0", count: numOfPlace)
            s = s.replacingOccurrences(of: ".\(zeroPlaceString)", with: "")
        }
        
        return s
    }
}

public extension String {
    func index(of string: String, options: String.CompareOptions = .literal) -> String.Index? {
        return range(of: string, options: options, range: nil, locale: nil)?.lowerBound
    }
    func indexes(of string: String, options: String.CompareOptions = .literal) -> [String.Index] {
        var result: [String.Index] = []
        var start = startIndex
        while let range = range(of: string, options: options, range: start..<endIndex, locale: nil) {
            result.append(range.lowerBound)
            start = range.upperBound
        }
        return result
    }
    func ranges(of string: String, options: String.CompareOptions = .literal) -> [Range<String.Index>] {
        var result: [Range<String.Index>] = []
        var start = startIndex
        while let range = range(of: string, options: options, range: start..<endIndex, locale: nil) {
            result.append(range)
            start = range.upperBound
        }
        return result
    }
    public func IsDigit() -> Bool {
        let s = self.replacingOccurrences(of: ",", with: "")
        let num = Float(s)
        
        return (num != nil)
    }
    
    public func toSqliteDate(datePart:Bool = true, timePart:Bool = true) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: DispEn.localeSym)
        dateFormatter.dateFormat = "\(datePart ? "yyyy-MM-dd" : "") \(timePart ? " HH:mm:ss" : "")".trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let convertedDate = dateFormatter.date(from: self)!
        return convertedDate
    }
    
    public func toDate(datePart:Bool = true, timePart:Bool = true) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: DispEn.localeSym)
        dateFormatter.dateFormat = "\(datePart ? "dd-MM-yyyy" : "") \(timePart ? " HH:mm:ss" : "")".trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let convertedDate = dateFormatter.date(from: self)!
        return convertedDate
    }
}

public extension Date {
    public func toString(_ isEnDate: Bool) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: isEnDate ? DispEn.localeSym : TxtLocale.localeSym)
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let convertedDate = dateFormatter.string(from: self)
        return convertedDate
    }
    
    public func toDisplayString(datePart:Bool = true, timePart:Bool = true) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: TxtLocale.localeSym)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "\(datePart ? "dd/MM/yyyy" : "") \(timePart ? " HH.mm.ss" : "")".trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let convertedDate = dateFormatter.string(from: self)
        return convertedDate
    }
}

public extension UIImage {
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}

public extension UIButton {
    public func underlineButton(text: String? = nil) {
        var txt = text
        if (txt == nil) {
            txt = self.currentTitle
        }
        let titleString = NSMutableAttributedString(string: txt!)
        titleString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: NSMakeRange(0, txt!.characters.count))
        self.setAttributedTitle(titleString, for: .normal)
    }
}

public extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}

public extension Bundle {
    
    var releaseVersionNumber: String? {
        return self.infoDictionary?["CFBundleShortVersionString"] as? String
    }
    
    var buildVersionNumber: String? {
        return self.infoDictionary?["CFBundleVersion"] as? String
    }
    
}
