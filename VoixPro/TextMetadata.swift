//
//  TxtLocale.swift
//  VoixPRO
//
//  Created by Song on 11/27/2559 BE.
//  Copyright © 2559 Song. All rights reserved.
//

import Foundation

/// English display text container
let DispEn = TxtEN()
/// Thai display text container
let DispTH = TxtTH()
/// Current display text container default ot English
var TxtLocale: TxtLocaleAbs = DispEn

/// Prototype for a languange display text containner
protocol TxtLocaleAbs {
    // Language symbol
    var symbol: String {get}
    var localeSym: String {get}
    var SKSLanguage: String {get}
    
    // words to compare voice process result
    var noWords: [String] {get}
    var confirmWords: [String] {get}
    var saveAndBackWords: [String] {get}
    var resetWords: [String] {get}
    
    // Transaction report
    var cellLot: String {get}
    var cellBin: String {get}
    
    // Put away & Pick transaction
    var cellDocNo: String {get}
    var cellItemNo: String {get}
    var cellBinShelf: String {get}
    var cellLotNo: String {get}
    
    // Network
    var noNetworkConnection: String {get}
    var serverNotReach: String {get}
    var serverResponseFailed: String {get}
    var speechKitServerFailed: String {get}

    // Navigation title
    var loginTitle: String {get}
    var mainMenuTitle: String {get}
    var dailyTransTitle: String {get}
    var putAwayTitle: String {get}
    var pickTitle: String {get}
    var auditStockTitle: String {get}
    var putAwayTransTitle: String {get}
    var pickTransTitle: String {get}
    var auditStockTransTitle: String {get}
    
    // Login
    var emptyUsername: String {get}
    var usernamePlaceHolder: String {get}
    var passwordPlaceHolder: String {get}
    var forgotPasswordTitle: String {get}
    var loginButtonTitle: String {get}
    
    // Main menu
    var welcomeTitle: String {get}
    var reportButtonTitle: String {get}
    var logoutButtonTitle: String {get}
    // Main menu & Daily Trans
    var putAwayButtontitle: String {get}
    var auditButtonTitle: String {get}
    var pickButtonTitle: String {get}
    var backButtonTitle: String {get}
    // Main report
    var mainReportTitle: String {get}
    
    // Transaction Insert Audit & Pick & PutAway
    var scanBabrcodeTitle: String {get}
    var lotPlaceHolder: String {get}
    var binPlaceHolder: String {get}
    var inputQuantityTitle: String {get}
    var quantityPlaceHolder: String {get}
    var yesButtonTitle: String {get}
    var nobuttonTitle: String {get}
    var confirmButtontitle: String {get}
    var invalidLotNumber: String {get}
    var invalidBinCode: String {get}
    var invalidQuantity: String {get}
    var emptyLotNumber: String {get}
    var emptyBinCode: String {get}
    var emptyQuantity: String {get}
    var emptyData: String {get}
    var listening: String {get}
    var processing: String {get}
    var itemNoPlaceHolder: String {get}
    var emptyItemNo: String {get}
    
}

/// English display text
struct TxtEN : TxtLocaleAbs {
    // Language symbol
    var symbol = "EN"
    var localeSym = "en-US"
    var SKSLanguage = "eng-USA"
    
    // words to compare voice process result
    var noWords = ["No", "Nope", "Wrong"]
    var confirmWords = ["Yes", "Correct", "Right"]
    var saveAndBackWords = ["Confirm", "Save"]
    var resetWords = ["Reset", "Cancel", "Clear"]
    
    // Transaction report
    var cellLot = "Lot Number : %@"
    var cellBin = "Bin Number : %@"
    
    // Put away & Pick transaction
    var cellDocNo = "DocNo/OrderNo : %@/%@"
    var cellItemNo = "ItemNo/Name : %@/%@"
    var cellBinShelf = "Bin/Shelf : "
    var cellLotNo = "LotNo"
    
    // Network
    var noNetworkConnection = "No network connection detected !"
    var serverNotReach = "Can't reach the server url : %@"
    var serverResponseFailed = "Invalid server response data !\r\nDetail: %@"
    var speechKitServerFailed = "Failed to initialize SpeechKit session."
    
    // Navigation title
    var loginTitle = "Log In"
    var mainMenuTitle = "Main Menu"
    var dailyTransTitle = "Transactions Report"
    var putAwayTitle = "Put Away"
    var pickTitle = "Pick"
    var auditStockTitle = "Audit Stock"
    var putAwayTransTitle = "Put Away Transactions"
    var pickTransTitle = "Pick Transactions"
    var auditStockTransTitle = "Audit Stock Transactions"
    
    // Login
    var emptyUsername = "Username must not empty !!!"
    var usernamePlaceHolder = "Username"
    var passwordPlaceHolder = "Password"
    var forgotPasswordTitle = "Forgot password?"
    var loginButtonTitle = "Login"
    
    // Main menu
    var welcomeTitle = "Welcome"
    var reportButtonTitle = "Report"
    var logoutButtonTitle = "Logout"
    // Main menu & Daily Trans
    var putAwayButtontitle = "Put Away"
    var auditButtonTitle = "Audit Stock"
    var pickButtonTitle = "Pick"
    var backButtonTitle = "Back"
    // Main report
    var mainReportTitle = "Main Report"
    
    // Transaction Insert Audit & Pick & PutAway
    var scanBabrcodeTitle = "Scan Barcode"
    var lotPlaceHolder = "Lot Number"
    var binPlaceHolder = "Bin Number"
    var inputQuantityTitle = "Input Quantity"
    var quantityPlaceHolder = "Quantity"
    var yesButtonTitle = "Yes"
    var nobuttonTitle = "No"
    var confirmButtontitle = "Confirm"
    var invalidLotNumber = "Lot number is mismatch"
    var invalidBinCode = "Bin Number is mismatch"
    var invalidQuantity = "Quantity input must less than or equal %.2f"
    var emptyLotNumber = "Lot number is invalid"
    var emptyBinCode = "Bin Number is invalid"
    var emptyQuantity = "Quantity is invalid"
    var emptyData = "Data is empty !!!"
    var listening = "Listening..."
    var processing = "Processing..."
    var itemNoPlaceHolder = "Item No"
    var emptyItemNo = "Item No is invalid"
}

/// Thai dispaly text
struct TxtTH : TxtLocaleAbs {
    // Language symbol
    var symbol = "TH"
    var localeSym = "th-TH"
    var SKSLanguage = "tha-THA"
    
    // words to compare voice process result
    var noWords = ["ไม่ใช่", "ผิด"]
    var confirmWords = ["ใช่", "ถูก", "ถูกต้อง"]
    var saveAndBackWords = ["ยืนยัน", "บันทึก", "เซฟ"]
    var resetWords = ["เริ่มใหม่", "ยกเลิก"]
    
    // Transaction report
    var cellLot = "เลขที่ลอต : %@"
    var cellBin = "เลขที่บิน : %@"
    
    // Put away & Pick transaction
    var cellDocNo = "DocNo/OrderNo : %@/%@"
    var cellItemNo = "ItemNo/Name : %@/%@"
    var cellBinShelf = "Bin/Shelf : "
    var cellLotNo = "LotNo"
    
    // Network
    var noNetworkConnection = "ไม่สารถเชื่อมต่ออินเทอร์เนต !"
    var serverNotReach = "ไม่สามารถติดต่อเซิฟเวอร์ : %@"
    var serverResponseFailed = "เซิฟเวอร์ทำงานไม่ถูกต้อง !\r\nข้อมูล: %@"
    var speechKitServerFailed = "เซิฟเวอร์ SpeechKit ทำงานผิดพลาด"
    
    // Navigation title
    var loginTitle = "เข้าสู่ระบบ"
    var mainMenuTitle = "หน้าหลัก"
    var dailyTransTitle = "ข้อมูลการดำเนินการ"
    var putAwayTitle = "จัดเก็บ"
    var pickTitle = "จ่าย"
    var auditStockTitle = "ตรวจสอบ"
    var putAwayTransTitle = "รายการจัดเก็บ"
    var pickTransTitle = "รายการจ่าย"
    var auditStockTransTitle = "รายการตรวจสอบ"
    
    // Login
    var emptyUsername = "ชื่อผู้ใช้ต้องไม่เป็นค่าว่าง"
    var usernamePlaceHolder = "ชื่อผู้ใช้งาน"
    var passwordPlaceHolder = "รหัสผ่าน"
    var forgotPasswordTitle = "ลืมรหัสผ่าน?    "
    var loginButtonTitle = "เข้าสู่ระบบ"
    
    // Main menu
    var welcomeTitle = "ยินดีต้อนรับ"
    var reportButtonTitle = "รายงาน"
    var logoutButtonTitle = "ออกจากระบบ"
    // Main menu & Daily Trans
    var putAwayButtontitle = "จัดเก็บ"
    var auditButtonTitle = "ตรวจสอบ"
    var pickButtonTitle = "จ่าย"
    var backButtonTitle = "ย้อนกลับ"
    // Main report
    var mainReportTitle = "รายงานหลัก"
    
    // Transaction Insert Audit & Pick & PutAway
    var scanBabrcodeTitle = "ข้อมูลการสแกน"
    var lotPlaceHolder = "เลขที่ลอต"
    var binPlaceHolder = "เลขที่บิน"
    var inputQuantityTitle = "ข้อมูลจำนวน"
    var quantityPlaceHolder = "จำนวน"
    var yesButtonTitle = "ใช่"
    var nobuttonTitle = "ไม่ใช่"
    var confirmButtontitle = "บันทึก"
    var invalidLotNumber = "เลขที่ลอตไม่ถูกต้อง"
    var invalidBinCode = "เลขที่บินไม่ถูกต้อง"
    var invalidQuantity = "จำนวนต้องน้องกว่าหรือเท่ากับ %.2f"
    var emptyLotNumber = "เลขที่ลอตไม่ถูกต้อง"
    var emptyBinCode = "เลขที่บินไม่ถูกต้อง"
    var emptyQuantity = "จำนวนไม่ถูกต้อง"
    var emptyData = "ไม่มีข้อมูลที่จะบันทึก"
    var listening = "กำลังฟัง…"
    var processing = "กำลังประมวลผล…"
    var itemNoPlaceHolder = "เลขที่สินค้า"
    var emptyItemNo = "เลขที่สินค้าไม่ถูกต้อง"
}
