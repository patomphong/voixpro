//
//  AddTransactionViewController.swift
//  VoixPRO
//
//  Created by Song on 10/31/2559 BE.
//  Copyright © 2559 Song. All rights reserved.
//

import UIKit
import SpeechKit
import AVFoundation

class AddTransactionViewController : UIViewController, UITextFieldDelegate, SKTransactionDelegate, SKAudioPlayerDelegate, UINavigationControllerDelegate {
    
    // State Logic: IDLE -> LISTENING -> PROCESSING -> repeat
    enum SKSState {
        case SKSIdle
        case SKSListening
        case SKSProcessing
    }
    
    /// Transaction voice state
    ///
    /// - none:
    /// - digit:
    /// - confirm:
    enum VoiceInputState {
        case none
        case digit
        case confirm
    }
    
    // User interface
    @IBOutlet var logviewLabel: UILabel!
    @IBOutlet var itemNoText: UITextField!
    @IBOutlet var lotNumberText: UITextField!
    @IBOutlet var binNumberText: UITextField!
    @IBOutlet var quantityText: UITextField!
    @IBOutlet var yesButton: UIButton!
    @IBOutlet var noButton: UIButton!
    @IBOutlet var saveButton: UIButton!
    @IBOutlet var resetButton: UIButton!
    @IBOutlet var langButton: UIButton!
    @IBOutlet var scanBarcodeTitle: UILabel!
    @IBOutlet var inputQuantityTitle: UILabel!
    @IBOutlet var indicator: UIActivityIndicatorView!
    @IBOutlet var scanBarcodeItemNumberButton: UIButton!
    @IBOutlet var scanBarcodeBinNumberButton: UIButton!
    
    // pick & put away data
    var docNo: String = ""
    var itemNo: String = ""
    
    // Current screen showing
    var screenType: screenType = .none
    var originalSelectedData: PutPickTran? = nil
    var selectedData: PutPickTran? = nil
    var currentAuditData = auditStockData()
    
    // Settings
    var languageSetting: String!
    var recognitionType: String!
    var endpointer: SKTransactionEndOfSpeechDetection!
    
    var skSession:SKSession?
    var skTransaction:SKTransaction?
    
    var state = SKSState.SKSIdle
    
    var volumePollTimer: Timer?
    
    @nonobjc static var lastResultText: String = ""
    var isQuantityVoiceEdit = false
    var qttVoiceState = VoiceInputState.none
    var isVoiceProcessing = false
    var isSaveRequest = false
    var willExit = false
    var isSaved = false
    
    // TTS variables
    var speakLocaleVoice: AVSpeechSynthesisVoice? = nil
    let voiceRate = Float(0.40)
    let voiceVolume = Float(1.15)
    let voicePitch = Float(1.35)
    let synthesizer = AVSpeechSynthesizer()
    
    /// This language display setting function used with sub view controller to call out
    var localeFuncParent: LocaleSettingDelegate?
    
    var scanBarcodeType: scanBarcodeType = .none
    var scanBarcodeResult: String  = ""
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if (viewController == self) {
            isSaved = false
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    /// On keyboard show
    ///
    /// - Parameter sender: triggered control
    func keyboardWillShow(sender: NSNotification) {
        // Restore previous set if bin control is responder
        if binNumberText.isFirstResponder {
            self.view.frame.origin.y = 0
            return
        }
        
        // Restore previous set if itemNo control is responder
        if itemNoText.isFirstResponder {
            self.view.frame.origin.y = 0
            return
        }
        
        // Restore previous set if quantityText control is responder or in voice mode
        if (quantityText.isFirstResponder || isQuantityVoiceEdit) {
            // Calculate new y axis point on frame
            if let userInfo = sender.userInfo {
                let kbFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! CGRect)
                let qttFrame = yesButton.frame
                let qttBottom  = (qttFrame.origin.y + qttFrame.height + 10)
                
                if (kbFrame.origin.y < qttBottom) {
                    self.view.frame.origin.y = -(qttBottom - kbFrame.origin.y)
                }
                //print(endFrame)
            }
            return
        }
    }
    
    /// On keybaord hide
    ///
    /// - Parameter sender: triggered control
    func keyboardWillHide(sender: NSNotification) {
        // Restore to default view
        self.view.frame.origin.y = 0
    }
    
    /// Load view
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Stop input activity
        UIApplication.shared.beginIgnoringInteractionEvents()
        // Show loading indicator
        self.indicator.isHidden = false
        self.indicator.startAnimating()
        
        self.navigationController?.delegate = self
        //self.hideKeyboardWhenTappedAround()
        
        // Set voice confiuration
        recognitionType = SKTransactionSpeechTypeSearch
        endpointer = .short
        state = .SKSIdle
        skTransaction = nil
        
        // clear voice status
        logviewLabel.text = ""
        
        // Note that SO highlighting makes the new selector syntax (#selector()) look
        // like a comment but it isn't one
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil);
        
        // Initialize voice configuration with server
        DispatchQueue.global().async {
            var msg = ""
            let connected = connectedToNetwork()
            if (connected) {
                // Create a session
                self.skSession = SKSession(url: NSURL(string: SKSServerUrl) as URL!, appToken: SKSAppKey)
                
                if (self.skSession == nil) {
                    msg = TxtLocale.speechKitServerFailed
                } else {
                    
                    self.loadEarcons()
                }
                
            } else {
                msg = TxtLocale.noNetworkConnection
            }
            
            DispatchQueue.main.async {
                
                // set locale display screen
                self.setScreenDisplayAndControlsLocale()
//                #if DEBUG
//                    if (self.screenType != .auditStock) {
//                        self.itemNoText.text = self.originalSelectedData!.ItemNo
//                        self.lotNumberText.text = self.originalSelectedData!.Lot
//                        self.binNumberText.text = self.originalSelectedData!.BinCode
//                    }
//                #endif
                
                if !msg.isEmpty {
                    self.log(message: msg)
                }
                
                self.indicator.isHidden = true
                self.indicator.stopAnimating()
                UIApplication.shared.endIgnoringInteractionEvents()
            }
        }
        
        //tap to hide keyboard
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard(_:)))
        view.addGestureRecognizer(tap)
        view.isUserInteractionEnabled = true
    }
    
    func hideKeyboard(_ sender: UITapGestureRecognizer) {
        itemNoText.resignFirstResponder()
        lotNumberText.resignFirstResponder()
        binNumberText.resignFirstResponder()
        quantityText.resignFirstResponder()
    }
    
    @IBAction func languageTouchup(_ sender: AnyObject) {
        setScreenDisplayAndControlsLocale(true)
        cancel()
    }
    
    func setScreenDisplayAndControlsLocale(_ isSet: Bool = false) {
        Utilities.setScreenDisplayLocale(isSet, langButton, setControlsDisplayTitle: setControlsDisplayTitle)
    }
    
    func setControlsDisplayTitle(_ l: TxtLocaleAbs) {
        switch screenType {
        case .auditStock:
            self.navigationItem.title = l.auditStockTitle
            break
        case .pick:
            self.navigationItem.title = l.pickTitle
            break
        case .putAway:
            self.navigationItem.title = l.putAwayTitle
            break
        default:
            assert(false, "Invalid screen type")
            break
        }
        
        speakLocaleVoice = AVSpeechSynthesisVoice(language: l.localeSym)
        //speakLocaleVoice!.quality = .enhanced
        languageSetting = l.SKSLanguage
        itemNoText.placeholder = l.itemNoPlaceHolder
        lotNumberText.placeholder = l.lotPlaceHolder
        binNumberText.placeholder = l.binPlaceHolder
        quantityText.placeholder = l.quantityPlaceHolder
        scanBarcodeTitle.text = l.scanBabrcodeTitle
        inputQuantityTitle.text = l.inputQuantityTitle
        yesButton.setTitle(l.yesButtonTitle, for: UIControlState.normal)
        noButton.setTitle(l.nobuttonTitle, for: UIControlState.normal)
        saveButton.setTitle(l.confirmButtontitle, for: UIControlState.normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.setFirstResponderEmptyText()
        }
    }
    
    /// ON view move to other view
    ///
    /// - Parameter parent: parent view
    override func willMove(toParentViewController parent: UIViewController?) {
        if parent == nil {
            willExit = true
            // save to store
            if (!isSaveRequest) {
                addCurrentTextToDataStore()
            }
            // Cancel voice input
            cancel()
            state = .SKSIdle
            skSession = nil
            skTransaction = nil
            isVoiceProcessing = false
            isSaveRequest = false
            if let del = localeFuncParent {
                del.setScreenDisplayAndControlsLocale(false)
            }
            // Delay a bit for voice api clearing data
            usleep(100000)
        }
        // back to parent view
        super.willMove(toParentViewController: parent)
        // Force cancel all active operation
        OperationQueue.main.cancelAllOperations()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction private func allTtextFieldDidBeginEdit(_ sender: AnyObject) {
        /// Set background on current responder
        let ctrl = sender as! UITextField
        ctrl.backgroundColor = UIColor.yellow
        quantityText.backgroundColor = UIColor.white
        qttVoiceState = .none
        // Cancel previous voice input
        cancel()
        resetTransaction()
    }
    
    @IBAction func quantityTextDidBeginEdit(_ sender: AnyObject) {
        beginEditQuantity(false)
    }
    
    func beginEditQuantity(_ isVoice: Bool) {
        /// Set background on current responder
        quantityText.backgroundColor = UIColor.yellow
        
        // Hide yes/no button and resset voice input
        setYesNobuttonsStatus(hidden: true)
        qttVoiceState = .none
        cancel()
        isQuantityVoiceEdit = isVoice
        qttVoiceState = .digit
        resetTransaction(isVoice)
    }
    
    @IBAction func textFieldDidEndEditing(_ textField: UITextField) {
        // Set background to normal when not a responder
        textField.backgroundColor = UIColor.white
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // Filter quantity input text in number only
        if (textField == quantityText) {
            let aSet = NSCharacterSet(charactersIn:"0123456789.").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // force end edting triggered control
        textField.endEditing(true)
        textField.resignFirstResponder()
        
        // hide yes/no button
        setYesNobuttonsStatus(hidden: true)
        
        // set next responder (next text field)
        switch textField {
        case itemNoText:
            splitItemNoData();
            binNumberText.becomeFirstResponder()
            break
        case lotNumberText:
            binNumberText.becomeFirstResponder()
            break
        case binNumberText:
            beginEditQuantity(true)
            break
        case quantityText:
            quantityEndEdit()
            isQuantityVoiceEdit = false
            // End process loop if not audit stock input
            if (screenType != .auditStock) {
                yesButtonTouchup(yesButton)
            }
            break
        default:
            assert(false, "invalid text field control")
            break
        }
        
        return false
    }
    
    @IBAction func scanBarcodeTouchup(_ sender: AnyObject) {
        
        guard let tag = sender.tag else {
            return
        }
        
        switch tag {
        case 1:
            self.scanBarcodeType = .item
        case 2:
            self.scanBarcodeType = .bin
        default:
            self.scanBarcodeType = .none
        }
        
        self.performSegue(withIdentifier: "showScanQRCode", sender: sender)
        
        print("tag = \(tag)")
    }
    
    @IBAction func noButtonTouchup(_ sender: AnyObject) {
        setYesNobuttonsStatus(hidden: true)
        AddTransactionViewController.lastResultText = ""
        qttVoiceState = .digit
        quantityText.text = ""
    }
    
    @IBAction func yesButtonTouchup(_ sender: AnyObject) {
        // cancel voice input
        cancel()
        
        // Check is audit stock screen
        if screenType == .auditStock {
            AddTransactionViewController.lastResultText = quantityText.text!
            setYesNobuttonsStatus(hidden: true)
            quantityEndEdit()
            // Save data without exit
            saveData(false)
        } else {
            // Save data with exit
            willExit = true
            saveData(true)
        }
    }
    
    @IBAction func saveTouchup(_ sender: AnyObject) {
        isSaveRequest = true
        willExit = true
        DispatchQueue.main.async {
            self.saveData(true)
        }
    }
    
    //MARK - Helpers
    
    /// Select a control that empty text by top to down
    func setFirstResponderEmptyText() {
        DispatchQueue.main.async {
            if (self.itemNoText.text!.isEmpty) {
                if (!self.itemNoText.isFirstResponder) {
                    self.itemNoText.becomeFirstResponder()
                }
            } else if (self.binNumberText.text!.isEmpty) {
                if (!self.binNumberText.isFirstResponder) {
                    self.binNumberText.becomeFirstResponder()
                }
            } else if (self.quantityText.text!.isEmpty) {
                if (!self.quantityText.isFirstResponder) {
                    if (self.isQuantityVoiceEdit) {
                        self.beginEditQuantity(true)
                    } else {
                        self.quantityText.becomeFirstResponder()
                    }
                }
            }
        }
    }
    
    /// Check current data is ready to save
    ///
    /// - Returns: true if ready to save
    func isReadyToSave() -> Bool {
        // All data not empty except bin data
        return (quantityText.text!.IsDigit()) && !(itemNoText.text!.isEmpty || /*lotNumberText.text!.isEmpty ||*/ binNumberText.text!.isEmpty || quantityText.text!.isEmpty)
    }
    
    /// Clear current screen data
    func clearData() {
        DispatchQueue.main.async {
            self.itemNoText.text = ""
            self.lotNumberText.text = ""
            self.binNumberText.text = ""
            self.quantityText.text = ""
        }
    }
    
    /// Validate current input data
    ///
    /// - Returns: error message if failed validation
    func validateInputData() -> String {
        var result = ""
        
        /// Current input data
        let itemNo = itemNoText.text!
        let lot = lotNumberText.text!
        let bin = binNumberText.text!
        let q = (quantityText.text!.IsDigit() ? Double(quantityText.text!.replacingOccurrences(of: ",", with: "")) : nil)
        
        // Check in each screen validation
        switch screenType {
        case .auditStock:
            if (itemNo.isEmpty) {
                result = TxtLocale.emptyItemNo
            } else if (bin.isEmpty){
                result = TxtLocale.emptyBinCode
            } else if (q == nil) {
                result = TxtLocale.emptyQuantity
            }
            break
        case .pick:
            if (itemNo.isEmpty) {
                result = TxtLocale.emptyItemNo
            } else if (itemNo != originalSelectedData!.ItemNo){
                result = TxtLocale.emptyItemNo
            } else if (!lot.isEmpty && lot != originalSelectedData!.Lot){
                result = TxtLocale.invalidLotNumber
            } else if (bin != originalSelectedData!.BinCode){
                result = TxtLocale.invalidBinCode
            } else if (q != nil && q! > originalSelectedData!.Quantity) {
                result = String(format: TxtLocale.invalidQuantity, originalSelectedData!.Quantity)
            }
            break
        case .putAway:
            if (itemNo.isEmpty) {
                result = TxtLocale.emptyItemNo
            } else if (itemNo != originalSelectedData!.ItemNo){
                result = TxtLocale.emptyItemNo
            } else if (!lot.isEmpty && lot != originalSelectedData!.Lot){
                result = TxtLocale.invalidLotNumber
            } else if (q != nil && q! > originalSelectedData!.Quantity) {
                result = String(format: TxtLocale.invalidQuantity, originalSelectedData!.Quantity)
            }
            break
        default:
            assert(false, "Invalid screen type")
            result = "Invalid screen type"
            break
        }
        
        return result
    }
    
    /// Save current data
    ///
    /// - Parameter isExit:
    func saveData(_ isExit: Bool) {
        //validate data
        let msgResult = validateInputData()
        if !msgResult.isEmpty {
            let alertCotroller = Utilities.makeAlert("", msg: msgResult, cancelTitle: "OK", handler: { action in
                self.quantityText.text = ""
                self.beginEditQuantity(true)
                self.setFirstResponderEmptyText()
            })
            self.present(alertCotroller, animated: true, completion: nil)
            return
        }
        
        // save to store
        addCurrentTextToDataStore()
        
        if (isExit) {
            view.endEditing(true)
            
            let networkConnected = connectedToNetwork()
            DispatchQueue.main.async {
                if (networkConnected) {
                    //indicator and not allowed input
                    UIApplication.shared.beginIgnoringInteractionEvents()
                    self.indicator.isHidden = false
                    self.indicator.startAnimating()
                    UIApplication.shared.isNetworkActivityIndicatorVisible = true
                    // Post data to server and exit this view
                    self.postDataToServer(true)
                } else {
                    self.processSaveAuditStockResult(false, message: TxtLocale.noNetworkConnection)
                }
            }
        } else {
            // Check ready to save
            if (isReadyToSave()) {
                // clear data
                clearData()
            }
            isSaved = false
            DispatchQueue.main.async {
                self.setFirstResponderEmptyText()
            }
        }    }
    
    /// Process result data
    ///
    /// - Parameters:
    ///   - succeeded: succeeded status
    ///   - message: message
    func processSaveAuditStockResult(_ succeeded: Bool, message: String = "") {
        // hide indicator
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        if (succeeded) {
            // Clear data
            switch self.screenType {
            case .auditStock:
                // Delete from database
                DBUtils.DeleteAuditStockInput(nil, true)
                break
            case .pick:
                break
            case .putAway:
                break
            default:
                assert(false, "Invalid screen type")
                return
            }
            self.indicator.isHidden = true
            self.indicator.stopAnimating()
            UIApplication.shared.endIgnoringInteractionEvents()
        } else{
            self.indicator.isHidden = true
            self.indicator.stopAnimating()
            UIApplication.shared.endIgnoringInteractionEvents()
            // Show alert with error message
            let alertCotroller = Utilities.makeAlert("", msg: message, cancelTitle: "OK")
            self.present(alertCotroller, animated: true, completion: nil)
        }
        print("success: \(succeeded), msg: \(message)")
    }
    
    /// Post data to web service
    ///
    /// - Parameter isExitOnSuccess:
    func postDataToServer(_ isExitOnSuccess: Bool = false) {
        /// Get error message from web service
        ///
        /// - Parameter m: description
        /// - Returns: formatted string
        func getServerErrMsg (_ m: String) -> String {
            return String(format:TxtLocale.serverResponseFailed, m)
        }
        
        var servicePostData: String
        var storeData: NSDictionary
        
        switch screenType {
        case .auditStock:
            var arr: [NSDictionary] = []
            var list: [auditStockData] = []
            // Get current data from data base for audit stock
            DBUtils.GetAuditStockInput(&list)
            
            // Check exiting data
            if (list.count < 1) {
                self.processSaveAuditStockResult(false, message: TxtLocale.emptyData)
                return
            }
            
            // Create audit stock data for json string
            for d in list {
                arr.append(["PhysDate": d.PhysDate.toString(true), "Lot": d.Lot, "BinCode": d.BinCode, "Quantity": d.Quantity, "ShelfNo": d.ShelfNo, "UserID": d.UserID, "ItemNo": d.ItemNo])
            }
            servicePostData = postAuditStock
            storeData = ["SaveTo": "AuditStock", "data": arr]
            break
        case .pick:
            // Create pick transaction data for json string
            let docLineNo = (originalSelectedData!.DocLineNo.isEmpty ? 0.0 : Float(originalSelectedData!.DocLineNo))!
            let d = selectedData!
            let pickData: NSDictionary = ["DocNo": originalSelectedData!.DocNo, "DocLineNo": docLineNo, "ItemNo": originalSelectedData!.ItemNo, "LotNo": d.Lot, "BinCode": d.BinCode, "ConfirmedQuantity": d.Quantity, "UserID": d.UserID]
            servicePostData = postPick
            storeData = ["SaveTo": "Pick", "data": pickData]
            break
        case .putAway:
            // Create put away transaction data for json string
            let docLineNo = (originalSelectedData!.DocLineNo.isEmpty ? 0.0 : Float(originalSelectedData!.DocLineNo))!
            let d = selectedData!
            let putData: NSDictionary = ["DocNo": originalSelectedData!.DocNo, "DocLineNo": docLineNo, "ItemNo": originalSelectedData!.ItemNo, "LotNo": d.Lot, "BinCode": d.BinCode, "ConfirmedQuantity": d.Quantity, "UserID": d.UserID]
            servicePostData = postPutAway
            storeData = ["SaveTo": "PutAway", "data": putData]
            break
        default:
            assert(false, "Invalid screen type")
            return
        }
        
        // Create post data
        var postData: Data
        do {
            postData = try JSONSerialization.data(withJSONObject: storeData, options: [])
        } catch {
            return
        }
        
        // Create rest manager
        let rest = RestManager(host: apiHost)
        
        // Set network activity on
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        // Create data for post
        let stringData = String.init(data: postData, encoding: .utf8)
        let postDataString = "Param=\(stringData!)"
        let postData2 = NSMutableData(data: postDataString.data(using: String.Encoding.utf8)!)
        
        // Post data to web service
        let result = rest.post(servicePostData, postJsonData: postData2) { (data, response, error) in
            
            guard error == nil else {
                self.processSaveAuditStockResult(false, message: String(format: TxtLocale.serverNotReach, postAuditStock))
                print(error!)
                return
            }
            guard let responseData = data else {
                self.processSaveAuditStockResult(false, message: getServerErrMsg("Error: did not receive data"))
                return
            }
            
            // parse the result as JSON, since that's what the API provides
            do {
                guard let resonseDict = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: AnyObject]
                    else {
                        self.processSaveAuditStockResult(false, message: getServerErrMsg("Could not get JSON from responseData as dictionary"))
                        return
                }
                
                guard (resonseDict["message"] as? String) == nil else {
                    self.processSaveAuditStockResult(false, message: resonseDict["message"] as! String)
                    return
                }
                
                guard let success = resonseDict["Success"] as? Bool else {
                    self.processSaveAuditStockResult(false, message: getServerErrMsg("Could not parse response data1"))
                    return
                }
                
                guard let resData = resonseDict["ResponseData"] as? [String: AnyObject] else {
                    self.processSaveAuditStockResult(false, message: getServerErrMsg("Could not parse response data2"))
                    return
                }
                
                guard let messages = resData["Message"] as? [String: AnyObject] else {
                    self.processSaveAuditStockResult(false, message: getServerErrMsg("Could not parse response data3"))
                    return
                }
                
                guard let message = messages[TxtLocale.symbol] as? String else {
                    self.processSaveAuditStockResult(false, message: getServerErrMsg("Could not parse response data4"))
                    return
                }
                
                self.processSaveAuditStockResult(success, message: message)
                
                if success {
                    PutAwayViewController.isNeedReload = true
                }
                
                if (success && isExitOnSuccess) {
                    // UI back
                    DispatchQueue.main.async {
                        self.resetTransaction()
                        
                        // navigate back
                        _ = self.navigationController?.popViewController(animated: true)
                    }
                }
                
            } catch  {
                self.processSaveAuditStockResult(false, message: getServerErrMsg("error parsing response from POST on \(postAuditStock)"))
                return
            }
        }
        
        if (!result.success) {
            processSaveAuditStockResult(false, message: getServerErrMsg(result.msg))
        }
        
    }
    
    /// Add current data to database
    func addCurrentTextToDataStore() {
        // Check ready to save
        if (isReadyToSave()) {
            let i = itemNoText == nil || itemNoText.text == nil ? "" : itemNoText.text!
            let l = lotNumberText == nil || lotNumberText.text == nil ? "" : lotNumberText.text!
            let b = binNumberText == nil || binNumberText.text == nil ? "" : binNumberText.text!
            var q: Double = 0
            if (quantityText != nil && quantityText.text != nil && quantityText.text!.IsDigit()) {
                q = Double(quantityText.text!.replacingOccurrences(of: ",", with: ""))!
            }
            let shelfNo = (UserInfos.shelfNo == nil ? "" : UserInfos.shelfNo!)
            
            switch screenType {
            case .auditStock:
                let d = currentAuditData
                d.isEntry = true
                d.Lot = l
                d.BinCode = b
                d.Quantity = q
                d.ShelfNo = shelfNo
                d.UserID = UserInfos.userName!
                d.ItemNo = i
                
                if !isSaved {
                    DBUtils.InsertAuditStockInput(d)
                    isSaved = true
                }
                
                break
            case .pick:
                selectedData!.isEntry = true
                selectedData!.Lot = l
                selectedData!.BinCode = b
                selectedData!.Quantity = q
                selectedData!.ShelfNo = shelfNo
                selectedData!.UserID = UserInfos.userName!
                
                //DBUtils.InsertPickStockInput(selectedData!)
                break
            case .putAway:
                selectedData!.isEntry = true
                selectedData!.Lot = l
                selectedData!.BinCode = b
                selectedData!.Quantity = q
                selectedData!.ShelfNo = shelfNo
                selectedData!.UserID = UserInfos.userName!
                
                //DBUtils.InsertPutAwayInput(selectedData!)
                break
            default:
                assert(false, "Invalid screen type")
                return
            }
        }
    }
    
    /// Split itemNo data with pipe(|) into 2 parts only
    func splitItemNoData() {
        let itemNo = itemNoText.text!
        let sepChars = CharacterSet.init(charactersIn: "|")
        let itemNoRaw = itemNo.components(separatedBy: sepChars)
        let itemNoSplit = itemNoRaw.filter({ (d) -> Bool in
            return !d.isEmpty
        })
        
        if (itemNoSplit.count > 0) {
            itemNoText.text = itemNoSplit[0]
        }
        
        if (itemNoSplit.count > 1) {
            lotNumberText.text = itemNoSplit[1]
        } else {
            lotNumberText.text!.removeAll()
        }
    }
    
    
    /// On qunatity control end editing
    func quantityEndEdit() {
        // reset voice input
        qttVoiceState = .none
        quantityText.backgroundColor = UIColor.white
        if (isQuantityVoiceEdit) {
            
        } else {
            // not using voice then save data
            saveData(false)
        }
        // Check is not ready to save then set to first empty control
        if (!isReadyToSave()) {
            self.setFirstResponderEmptyText()
        }
    }
    
    /// Set string to quantity control
    ///
    /// - Parameter text: string input
    func setTextToCurrentControl(text: String) {
        DispatchQueue.main.async {
            self.quantityText.text = text
            // If text not empty then speak the text
            if !text.isEmpty {
                self.SpeakText(text: text)
            }
        }
        // Show yes/no button to confirm
        setYesNobuttonsStatus(hidden: false)
    }
    
    /// Set show/hide for yes/no button
    ///
    /// - Parameter hidden:
    func setYesNobuttonsStatus(hidden: Bool) {
        DispatchQueue.main.async {
            self.yesButton.isHidden = hidden
            self.noButton.isHidden = hidden
        }
    }
    
    /// Log status and display to voice status if not a debug information
    ///
    /// - Parameters:
    ///   - message:
    ///   - isDebug: <#isDebug description#>
    func log(message: String, isDebug: Bool = false) {
        debugPrint(message)
        if (!isDebug) {
            DispatchQueue.main.async {
                self.logviewLabel?.text = message + "\r\n"
            }
        }
    }
    
    @IBAction func unwindToAddTransactionVC(segue:UIStoryboardSegue) {
        
        print("unwindToAddTransactionVC")
        
        if self.scanBarcodeResult != "" {
        
            switch self.scanBarcodeType {
            case .item:
                self.itemNoText.text = self.scanBarcodeResult
                break
            case .bin:
                self.binNumberText.text = self.scanBarcodeResult
                //self.quantityText.becomeFirstResponder()
                
                self.binNumberText.endEditing(true)
                
                if (!self.binNumberText.isFirstResponder) {
                    self.binNumberText.becomeFirstResponder()
                }
                
                beginEditQuantity(true)
                break
            default:
                assert(false, "Invalid scan barcode type")
                return
            }
            
            self.scanBarcodeType = .none
            self.scanBarcodeResult = ""
            
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "showScanQRCode" {
            let controller = segue.destination as! ScanBarCodeViewController
            controller.screenType = self.screenType
            controller.scanBarcodeType = self.scanBarcodeType
        }
    }
}
