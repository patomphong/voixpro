//
//  RestManager.swift
//  VoixPRO
//
//  Created by Song on 11/21/2559 BE.
//  Copyright © 2559 Song. All rights reserved.
//

import Foundation

class RestManager {
    // MARK: Using NSURLSession
    
    // server endpoint
    let host: String
    
    init(host: String) {
        self.host = host
    }
    
    func get(_ apiRoute: String, customCompletionHandler: @escaping (Data?, URLResponse?, Error?) -> Swift.Void) {
        let apiEndpoint = host + apiRoute
        guard let url = URL(string: apiEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: customCompletionHandler)

        task.resume()
    }
    
    func post(_ apiRoute: String, postJsonData: AnyObject, customCompletionHandler: @escaping (Data?, URLResponse?, Error?) -> Swift.Void) -> (success: Bool, msg: String) {
        let apiEndpoint: String = host + apiRoute
        guard let todosURL = URL(string: apiEndpoint) else {
            return (success: false, msg: "Error: cannot create URL")
        }
        var urlRequestPost = URLRequest(url: todosURL)
        urlRequestPost.timeoutInterval = TimeInterval(20)
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache"
        ]
        urlRequestPost.httpMethod = "POST"
        urlRequestPost.allHTTPHeaderFields = headers
        
//        urlRequestPost.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        if (postJsonData is Data) {
            urlRequestPost.httpBody = postJsonData as? Data
        } else {
            do {
                urlRequestPost.httpBody = try JSONSerialization.data(withJSONObject: postJsonData, options: .prettyPrinted)
            } catch {
                return (success: false, msg: "Error: cannot create JSON from todo")
            }
        }
        
        let task = URLSession.shared.dataTask(with: urlRequestPost, completionHandler: customCompletionHandler)
        task.resume()
        
        return (success: true, msg: "")
    }
}
