//
//  AddTransVoiceControls.swift
//  VoixPRO
//
//  Created by Song on 12/17/2559 BE.
//  Copyright © 2559 Song. All rights reserved.
//

import Foundation
import UIKit
import SpeechKit
import AVFoundation

extension AddTransactionViewController {
    
    // MARK: - ASR Actions
    func toggleRecognition() {
        if willExit {
            willExit = false
            return
        }
        DispatchQueue.main.async {
            switch self.state {
            case .SKSIdle:
                self.recognize()
            case .SKSListening:
                self.stopRecording()
            case .SKSProcessing:
                self.cancel()
            }
        }
    }
    
    func resetTransaction(_ withToggle: Bool = false) {
        DispatchQueue.main.async {
            
            if (self.skTransaction != nil && self.state != .SKSIdle) {
                self.cancel()
            }
            self.state = .SKSIdle
            //self.skSession = nil
            self.skTransaction = nil
            self.isVoiceProcessing = false
            
            if withToggle {
                self.toggleRecognition()
            }
        }
    }
    
    func recognize() {
        if (isVoiceProcessing) {
            return
        }
        
        if (isVoiceProcessing || skSession == nil) {
            if (connectedToNetwork()) {
                // Create a session
                skSession = SKSession(url: NSURL(string: SKSServerUrl) as URL!, appToken: SKSAppKey)
                
                if (skSession == nil) {
                    resetTransaction()
                    logviewLabel.text = TxtLocale.speechKitServerFailed
                    return
                }
            } else {
                resetTransaction()
                logviewLabel.text = TxtLocale.noNetworkConnection
                return
            }
            
        }
        // Start listening to the user.
        //open func recognize(withType type: String!, detection: SKTransactionEndOfSpeechDetection, language: String!, delegate: SKTransactionDelegate!) -> SKTransaction!
        if (skTransaction == nil) {
            skTransaction = skSession?.recognize(withType: recognitionType, detection: endpointer, language: languageSetting, delegate: self)
//            skTransaction = skSession!.recognizeWithType(recognitionType,
//                                                         detection: endpointer,
//                                                         language: languageSetting,
//                                                         delegate: self)
        }
    }
    
    func stopRecording() {
        if (skTransaction == nil) {
            return
        }
        // Stop recording the user.
        skTransaction!.stopRecording()
        
        // Disable the button until we received notification that the transaction is completed.
        
    }
    
    func cancel() {
        if (skTransaction == nil) {
            return
        }
        // Cancel the Reco transaction.
        // This will only cancel if we have not received a response from the server yet.
        skTransaction!.cancel()
    }
    
    // MARK: - SKTransactionDelegate
    func transactionDidBeginRecording(_ transaction: SKTransaction!) {
        log(message: TxtLocale.listening)
        isVoiceProcessing = true
        state = .SKSListening
    }
    
    func transactionDidFinishRecording(_ transaction: SKTransaction!) {
        log(message: TxtLocale.processing)
        state = .SKSProcessing
        
        //indicator and not allowed input
        UIApplication.shared.beginIgnoringInteractionEvents()
        self.indicator.isHidden = false
        self.indicator.startAnimating()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    @nonobjc static func containsVoiceWord(_ text: String) -> Bool {
        return AddTransactionViewController.lastResultText.caseInsensitiveCompare(text) == ComparisonResult.orderedSame
    }
    
    func transaction(_ transaction: SKTransaction!, didReceive recognition: SKRecognition!) {
        log(message: String(format: "didReceiveRecognition: %@", arguments: [recognition.text]), isDebug: true)
        AddTransactionViewController.lastResultText = recognition.text
    }
    
    func transaction(_ transaction: SKTransaction!, didFinishWithSuggestion suggestion: String) {
        log(message: String(format: "didFinishWithSuggestion: %@", arguments: [suggestion]), isDebug: true)
        
        DispatchQueue.main.async {
            self.finishedReceivedWords(true, suggestion, nil)
        }

        state = .SKSIdle
    }
    
    func transaction(_ transaction: SKTransaction!, didFailWithError error: Error!, suggestion: String) {
        log(message: String(format: "didFailWithError: %@. %@", arguments: [error.localizedDescription, suggestion]), isDebug: true)
        
        // Something went wrong. Ensure that your credentials are correct.
        // The user could also be offline, so be sure to handle this case appropriately.
        
        DispatchQueue.main.async {
            self.finishedReceivedWords(false, suggestion, error)
        }
        
        state = .SKSIdle
    }
    
    func finishedReceivedWords(_ success: Bool, _ suggestion: String, _ error: Error!) {
        
        let connected = connectedToNetwork()
        if (success) {
            processReceivedWord(suggestion)
        } else {
            
        }
        
        if (connected) {
            let r = success ? AddTransactionViewController.lastResultText.isEmpty : false
            if !willExit && (qttVoiceState != .none || r) {
                resetTransaction(true)
            } else {
                logviewLabel.text = ""
            }
        } else {
            logviewLabel.text = TxtLocale.noNetworkConnection
        }
        
        // hide indicator
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        self.indicator.isHidden = true
        self.indicator.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    func processReceivedWord(_ suggestion: String) {
        
        if (TxtLocale.saveAndBackWords.contains(where: AddTransactionViewController.containsVoiceWord)) {
            isSaveRequest = true
            qttVoiceState = .none
        }
        
        if (screenType != .auditStock) {
            if (TxtLocale.confirmWords.contains(where: AddTransactionViewController.containsVoiceWord)) {
                isSaveRequest = true
                qttVoiceState = .none
            }
        }
        
        switch qttVoiceState {
        case .digit:
            if (AddTransactionViewController.lastResultText.IsDigit()){
                setTextToCurrentControl(text: AddTransactionViewController.lastResultText)
                qttVoiceState = .confirm
            }
            
            AddTransactionViewController.lastResultText = ""
            
            break
        case .confirm:
            var confirm : Bool? = nil
            if (TxtLocale.confirmWords.contains(where: AddTransactionViewController.containsVoiceWord)) {
                confirm = true
            } else if (TxtLocale.noWords.contains(where: AddTransactionViewController.containsVoiceWord)) {
                confirm = false
            }
            
            if (confirm != nil) {
                if (confirm == true) {
                    AddTransactionViewController.lastResultText = quantityText.text!
                    quantityEndEdit()
                    saveData(false)
                } else {
                    AddTransactionViewController.lastResultText = ""
                    qttVoiceState = .digit
                    quantityText.text = ""
                }
                setYesNobuttonsStatus(hidden: true)
            } else {
                AddTransactionViewController.lastResultText = ""
            }
            break
        case .none:
            // other command
            if (isSaveRequest || TxtLocale.confirmWords.contains(where: AddTransactionViewController.containsVoiceWord)) {
                var dataCount = 0
                switch self.screenType {
                case .auditStock:
                    dataCount = DBUtils.GetAuditStockInputCount()
                    break
                case .pick:
                    dataCount = DBUtils.GetPickInputCount()
                    break
                case .putAway:
                    dataCount = DBUtils.GetPutAwayInputCount()
                    break
                default:
                    assert(false, "Invalid screen type")
                    return
                }
                
                if (isReadyToSave() || (isSaveRequest && dataCount > 0)) {
                    willExit = isSaveRequest
                    saveData(isSaveRequest)
                } else {
                    //validate data
                    let msgResult = validateInputData()
                    if !msgResult.isEmpty {
                        let alertCotroller = Utilities.makeAlert("", msg: msgResult, cancelTitle: "OK")
                        self.present(alertCotroller, animated: true, completion: nil)
                    } else {
                        setYesNobuttonsStatus(hidden: true)
                    }
                    
                    setFirstResponderEmptyText()
                }
                break
            }
            
            if (TxtLocale.resetWords.contains(where: AddTransactionViewController.containsVoiceWord)) {
                clearData()
                break
            }
            
            break
        }
    }
    
    func loadEarcons() {
        let mb = Bundle.main
        let startEarconPath = mb.path(forResource: "sk_start", ofType: "pcm")
        let stopEarconPath = mb.path(forResource: "sk_stop", ofType: "pcm")
        let errorEarconPath = mb.path(forResource: "sk_error", ofType: "pcm")
        let audioFormat = SKPCMFormat()
        audioFormat.sampleFormat = .signedLinear16
        audioFormat.sampleRate = 16000
        audioFormat.channels = 1
        
        skSession!.startEarcon = SKAudioFile(url: NSURL(fileURLWithPath: startEarconPath!) as URL!, pcmFormat: audioFormat)
        skSession!.endEarcon = SKAudioFile(url: NSURL(fileURLWithPath: stopEarconPath!) as URL!, pcmFormat: audioFormat)
        skSession!.errorEarcon = SKAudioFile(url: NSURL(fileURLWithPath: errorEarconPath!) as URL!, pcmFormat: audioFormat)
    }
    
    /************************* Text to Speech ****************************************************/
    func SpeakText(text: String) {
        let utterance = AVSpeechUtterance(string: String(text))
        utterance.voice = self.speakLocaleVoice
        utterance.rate = self.voiceRate
        utterance.volume = self.voiceVolume
        utterance.pitchMultiplier = self.voicePitch
        
        let uttDelay = 0.003
        utterance.preUtteranceDelay = uttDelay
        utterance.postUtteranceDelay = uttDelay
        
        self.synthesizer.speak(utterance)
    }
}
