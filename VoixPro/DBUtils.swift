//
//  DBUtils.swift
//  VoixPRO
//
//  Created by Song on 12/3/2559 BE.
//  Copyright © 2559 Song. All rights reserved.
//

import Foundation

class DBUtils {
    static var databasePath = fileUtilties.pathToFile("voixpro", ext: "sqlite3")
    static var dbConnection: FMDatabase? = nil
    
    class func GetDBSqlite() -> FMDatabase {
        
        if DBUtils.dbConnection == nil {
            if !FileManager.default.fileExists(atPath: databasePath) {
                assert(false, "Internal Database not found !!!")
                return FMDatabase()
            }
            DBUtils.dbConnection = FMDatabase(path: databasePath)
        }
        
        return DBUtils.dbConnection!
    }
    
    class func GetAuditStockInput(_ auditData: inout [auditStockData]) {
        BaseGetInput(&auditData, "auditStockInput", .auditStock)
    }
    
    class func GetPutAwayInput(_ putAwayData: inout [PutPickTran]) {
        //BaseGetInput(&putAwayData, "putAwayInput", .putAway)
    }
    
    class func GetPickInput(_ pickData: inout [PutPickTran]) {
        //BaseGetInput(&pickData, "pickInput", .pick)
    }
    
    private class func BaseGetInput<T>(_ auditData: inout [T], _ tableName: String, _ t: screenType) {
        func setData<U: TranData>(_ ad: inout U, _ results: FMResultSet?, _ sc: screenType) {
            ad.Lot = results!.string(forColumn: "Lot")
            ad.BinCode = results!.string(forColumn: "BinCode")
            ad.Quantity = results!.double(forColumn: "Quantity")
            ad.ShelfNo = results!.string(forColumn: "ShelfNo")
            ad.UserID = results!.string(forColumn: "UserID")
            ad.PhysDate = results!.date(forColumn: "PhysDate")
            ad.isEntry = results!.bool(forColumn: "isEntry")
            ad.ItemNo = results!.string(forColumn: "ItemNo")
        }
        let query = "select rowid,* from \(tableName) where userId = '\(UserInfos.userName!)' order by rowid DESC"
        let db = GetDBSqlite()
        
        if db.open() {
            let results:FMResultSet? = db.executeQuery(query, withArgumentsIn: nil)

            while (results!.next() == true) {
                switch t {
                case .auditStock:
                    var ad = auditStockData()
                    setData(&ad, results, t)
                    ad.rowId = Int(results!.int(forColumn: "rowid"))
                    auditData.append(ad as! T)
                    break
                case .putAway:
                    var ad = PutPickTran()
                    setData(&ad, results, t)
                    ad.DocLineNo = results!.string(forColumn: "DocLineNo")
                    auditData.append(ad as! T)
                    break
                case .pick:
                    var ad = PutPickTran()
                    setData(&ad, results, t)
                    ad.DocLineNo = results!.string(forColumn: "DocLineNo")
                    auditData.append(ad as! T)
                    break
                default:
                    assert(false, "Invalid screen type !!")
                    return
                }
                
            }
            db.close()
        } else {
            print("Error: \(db.lastErrorMessage()!)")
        }
    }
    
    class func GetAuditStockInputCount() -> Int {
        return BaseGetCount("auditStockInput")
    }
    
    class func GetPutAwayInputCount() -> Int {
        return 0 //BaseGetCount("putAwayInput")
    }
    
    class func GetPickInputCount() -> Int {
        return 0 //BaseGetCount("pickInput")
    }
    
    private class func BaseGetCount(_ tableName: String) -> Int {
        let query = "select count('x') as c from \(tableName)"
        let db = GetDBSqlite()
        
        var count = -10
        if db.open() {
            let results:FMResultSet? = db.executeQuery(query, withArgumentsIn: nil)
            
            while (results!.next() == true) {
                count = Int(results!.int(forColumn: "c"))
                break
            }
            db.close()
        } else {
            print("Error get: \(db.lastErrorMessage()!)")
        }
        
        return count
    }
    
    class func InsertAuditStockInput(_ aData: auditStockData) {
        BaseInsertInput(aData, "auditStockInput")
    }
    
    class func InsertPutAwayInput(_ aData: PutPickTran) {
        BaseInsertInput(aData, "putAwayInput")
    }
    
    class func InsertPickStockInput(_ aData: PutPickTran) {
        BaseInsertInput(aData, "pickInput")
    }
    
    private class func BaseInsertInput(_ aData: TranData, _ tableName: String) {
        let query = "INSERT INTO \(tableName) (Lot,BinCode,Quantity,ShelfNo,UserID,PhysDate,isEntry,DocLineNo,ItemNo)VALUES(?,?,?,?,?,?,?,?,?);"
        let db = GetDBSqlite()
        
        if db.open() {
            if !db.executeUpdate(query, withArgumentsIn: [aData.Lot, aData.BinCode, aData.Quantity, aData.ShelfNo, aData.UserID, aData.PhysDate, aData.isEntry, aData.DocLineNo, aData.ItemNo]) {
                print("Error insert \(tableName) : \(db.lastErrorMessage()!)")
            }
            db.close()
        } else {
            print("Error: \(db.lastErrorMessage()!)")
        }
    }
    
    class func DeleteAuditStockInput(_ d: auditStockData?, _ deleteAll: Bool = false) {
        var query = "DELETE FROM auditStockInput "
        
        if (deleteAll) {
            query.append(";")
        } else {
//            query.append("WHERE Lot = \"\(d!.Lot)\" AND BinCode = \"\(d!.BinCode)\" AND ShelfNo = \"\(d!.ShelfNo)\" Quantity = \(d!.Quantity);")
            query.append("WHERE rowid = \(d!.rowId);")

        }
        
        let db = GetDBSqlite()
        
        if db.open() {
            //            if !db.executeUpdate(query, withArgumentsIn: (deleteAll ? nil : [d!.Lot, d!.BinCode, d!.ShelfNo])) {
            //                print("Error delete \(tableName) : \(db.lastErrorMessage()!)")
            //            }
            if !db.executeUpdate(query, withArgumentsIn: nil) {
                print("Error delete auditStockInput : \(db.lastErrorMessage()!)")
            }
            db.close()
        } else {
            print("Error: \(db.lastErrorMessage()!)")
        }
    }
}
