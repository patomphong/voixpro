//
//  ReportTransViewController.swift
//  VoixPRO
//
//  Created by Song on 11/27/2559 BE.
//  Copyright © 2559 Song. All rights reserved.
//

import UIKit

class ReportTransViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating {
    
    @IBOutlet var transTable: UITableView!
    @IBOutlet var indicator: UIActivityIndicatorView!
    @IBOutlet var langButton: UIButton!
    
    // Current screen showing
    var screenType: screenType = .none
    
    let textCellIdentifier = "TextCell"
    
    var auditTransList: [auditStockData] = []
    var pickTransList: [PutPickTran] = []
    var putTransList: [PutPickTran] = []
    
    var filteredAuditTrans: [auditStockData] = []
    var filteredPutTrans: [PutPickTran] = []
    var filteredPickTrans: [PutPickTran] = []
    var resultSearchController = UISearchController()
    
    var localeFuncParent: LocaleSettingDelegate?
    
    override func willMove(toParentViewController parent: UIViewController?) {
        super.willMove(toParentViewController: parent)
        if parent == nil {
            // The back button was pressed or interactive gesture used
            if let del = localeFuncParent {
                del.setScreenDisplayAndControlsLocale(false)
            }
        }
    }
    
    override func viewDidLoad() {
        transTable.separatorInset.right = transTable.separatorInset.left;
        
        super.viewDidLoad()
        
        setScreenDisplayAndControlsLocale()
        
        indicator.isHidden = true
        
        self.resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            
            transTable.tableHeaderView = controller.searchBar
            
            return controller
        })()
        
        
        // Do any additional setup after loading the view, typically from a nib.
        transTable.delegate = self
        transTable.dataSource = self
        
        DispatchQueue.main.async {
            UIApplication.shared.beginIgnoringInteractionEvents()
            self.indicator.isHidden = false
            self.indicator.startAnimating()
            
            self.loadTransDataFromServer()
        }
    }
    
    @IBAction func languageTouchup(_ sender: AnyObject) {
        setScreenDisplayAndControlsLocale(true)
        self.transTable.reloadData()
    }
    
    func setScreenDisplayAndControlsLocale(_ isSet: Bool = false) {
        Utilities.setScreenDisplayLocale(isSet, langButton, setControlsDisplayTitle: setControlsDisplayTitle)
    }
    
    func setControlsDisplayTitle(_ l: TxtLocaleAbs) {
        switch screenType {
        case .auditStock:
            self.navigationItem.title = l.auditStockTransTitle
            break
        case .pick:
            self.navigationItem.title = l.pickTransTitle
            break
        case .putAway:
            self.navigationItem.title = l.putAwayTransTitle
            break
        default:
            assert(false, "Invalid screen type")
            break
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setScreenDisplayAndControlsLocale()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadTransDataFromServer() {
        func getServerErrMsg(_ m: String) -> String {
            return String(format:TxtLocale.serverResponseFailed, m)
        }
        
        var saveTo: String
        var dateColumn: String
        var lotColumn: String
        var apiString: String
        var quantityText: String
        
        switch self.screenType {
        case .auditStock:
            apiString = queryAuditStock
            saveTo = "AuditStock"
            dateColumn = "PhysDate"
            lotColumn = "Lot"
            quantityText = "Quantity"
            auditTransList = []
            filteredAuditTrans = []
            break
        case .pick:
            apiString = queryPick
            saveTo = "Pick"
            dateColumn = "ConfirmDate"
            lotColumn = "LotNo"
            quantityText = "ConfirmedQuantity"
            pickTransList = []
            filteredPickTrans = []
            break
        case .putAway:
            apiString = queryPutAway
            saveTo = "PutAway"
            dateColumn = "ConfirmDate"
            lotColumn = "LotNo"
            quantityText = "ConfirmedQuantity"
            putTransList = []
            filteredPutTrans = []
            break
        default:
            assert(false, "Invalid screen type")
            return
        }
        
        let paramString = "Param={\"SaveTo\":\"\(saveTo)\",\"UserID\":\"\(UserInfos.userName!)\"}"
        let postData = NSMutableData(data: paramString.data(using: String.Encoding.utf8)!)
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        self.loadDataNotSave()
        
        let rest = RestManager(host: apiHost)
        let result = rest.post(apiString, postJsonData: postData) { (data, response, error) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            guard error == nil else {
                self.processLoadTransResult(false, message: String(format: TxtLocale.serverNotReach, apiString))
                print(error!)
                return
            }
            
            guard let responseData = data else {
                self.processLoadTransResult(false, message: getServerErrMsg("Error: did not receive data"))
                return
            }
            
            // parse the result as JSON, since that's what the API provides
            do {
                guard let resonseDict = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: Any]
                    else {
                        self.processLoadTransResult(false, message: getServerErrMsg("Could not get JSON from responseData as dictionary"))
                        return
                }
                
                guard (resonseDict["message"] as? String) == nil else {
                    self.processLoadTransResult(false, message: resonseDict["message"] as! String)
                    return
                }
                
                guard let success = resonseDict["Success"] as? Bool else {
                    self.processLoadTransResult(false, message: getServerErrMsg("Could not parse response data1"))
                    return
                }
                
                if (success) {
                    guard let dataArr = resonseDict["Data"] as? [Any] else {
                        self.processLoadTransResult(false, message: getServerErrMsg("Could not parse response data2"))
                        return
                    }
                    for auditData in dataArr {
                        guard let auditDataDic = auditData as? [String: Any] else {
                            self.processLoadTransResult(false, message: getServerErrMsg("Could not parse response data3"))
                            return
                        }
                        guard let physDate = auditDataDic[dateColumn] as? String else {
                            self.processLoadTransResult(false, message: getServerErrMsg("Could not parse response data4"))
                            return
                        }
                        guard let lot = auditDataDic[lotColumn] as? String else {
                            self.processLoadTransResult(false, message: getServerErrMsg("Could not parse response data5"))
                            return
                        }
                        guard let binCode = auditDataDic["BinCode"] as? String else {
                            self.processLoadTransResult(false, message: getServerErrMsg("Could not parse response data6"))
                            return
                        }
                        guard let quantity = auditDataDic[quantityText] as? Double else {
                            self.processLoadTransResult(false, message: getServerErrMsg("Could not parse response data7"))
                            return
                        }
                        guard let itemNo = auditDataDic["ItemNo"] as? String else {
                            self.processLoadTransResult(false, message: getServerErrMsg("Could not parse response data6"))
                            return
                        }
                        var docLine = ""
                        var docNo = ""
                        if (self.screenType != .auditStock) {
                            guard let docNo1 = auditDataDic["DocNo"] as? String else {
                                self.processLoadTransResult(false, message: getServerErrMsg("Could not parse response data4"))
                                return
                            }
                            guard let dl = auditDataDic["DocLineNo"] as? Int else {
                                self.processLoadTransResult(false, message: getServerErrMsg("Could not parse response data8"))
                                return
                            }
                            docLine = String(dl)
                            docNo = docNo1
                        }
                        
                        switch self.screenType {
                        case .auditStock:
                            let trn = auditStockData()
                            trn.isEntry = false
                            trn.BinCode = binCode
                            trn.Lot = lot
                            trn.PhysDate = physDate.toDate()
                            trn.Quantity = quantity
                            trn.ShelfNo = ""
                            trn.UserID = UserInfos.userName!
                            trn.DocLineNo = docLine
                            trn.ItemNo = itemNo
                            trn.PhysDateText = trn.PhysDate.toDisplayString()
                            trn.amountText = quantity.toCurrecyDigits(numOfPlace: 2)
                            self.auditTransList.append(trn)
                            break
                        case .pick:
                            let trn = PutPickTran()
                            trn.BinCode = binCode
                            trn.Lot = lot
                            trn.PhysDate = physDate.toDate()
                            trn.Quantity = quantity
                            trn.ShelfNo = ""
                            trn.UserID = UserInfos.userName!
                            trn.DocLineNo = docLine
                            trn.DocNo = docNo
                            trn.ItemNo = itemNo
                            trn.createDateText = trn.PhysDate.toDisplayString()
                            trn.amountText = quantity.toCurrecyDigits(numOfPlace: 2)
                            self.pickTransList.append(trn)
                            break
                        case .putAway:
                            let trn = PutPickTran()
                            trn.BinCode = binCode
                            trn.Lot = lot
                            trn.PhysDate = physDate.toDate()
                            trn.Quantity = quantity
                            trn.ShelfNo = ""
                            trn.UserID = UserInfos.userName!
                            trn.DocLineNo = docLine
                            trn.DocNo = docNo
                            trn.ItemNo = itemNo
                            trn.createDateText = trn.PhysDate.toDisplayString()
                            trn.amountText = quantity.toCurrecyDigits(numOfPlace: 2)
                            self.putTransList.append(trn)
                            break
                        default:
                            assert(false, "Invalid screen type")
                            return
                        }
                    }
                }
                else
                {
                    switch self.screenType {
                    case .auditStock:
                        self.auditTransList = []
                        break
                    case .pick:
                        self.pickTransList = []
                        break
                    case .putAway:
                        self.putTransList = []
                        break
                    default:
                        assert(false, "Invalid screen type")
                        return
                    }
                    
                }
                
                self.processLoadTransResult(success, message: getServerErrMsg(""))
                
            } catch  {
                self.processLoadTransResult(false, message: String(format: TxtLocale.serverNotReach, apiString))
                return
            }
        }
        
        if (!result.success) {
            processLoadTransResult(false, message: result.msg)
        }
    }
    
    func loadDataNotSave() {
        switch self.screenType {
        case .auditStock:
            DBUtils.GetAuditStockInput(&self.auditTransList)
            break
        case .pick:
            DBUtils.GetPickInput(&self.pickTransList)
            break
        case .putAway:
            DBUtils.GetPutAwayInput(&self.putTransList)
            break
        default:
            assert(false, "Invalid screen type")
            return
        }
    }
    
    func processLoadTransResult(_ succeeded: Bool, message: String = "") {
        //sleep(4)
        DispatchQueue.main.async {
            
            // Reload the table
            self.transTable.reloadData()
            
            // hide indicator
            self.indicator.isHidden = true
            self.indicator.stopAnimating()
            UIApplication.shared.endIgnoringInteractionEvents()
            
            if (succeeded) {
                //self.performSegue(withIdentifier: "showMainMenu", sender: self)
            }
            else
            {
                let alertCotroller = Utilities.makeAlert("", msg: message, cancelTitle: "OK")
                self.present(alertCotroller, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: - UITextFieldDelegate Methods
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            
            switch self.screenType {
            case .auditStock:
                let tran: auditStockData
                if (self.resultSearchController.isActive) {
                    tran = filteredAuditTrans.remove(at: indexPath.row)
                }
                else {
                    tran = auditTransList.remove(at: indexPath.row)
                }
                if (tran.isEntry) {
                    DBUtils.DeleteAuditStockInput(tran)
                } else {
                    DeleteAuditStockServer(tran)
                }
                break
            case .pick:
                let tran: PutPickTran
                if (self.resultSearchController.isActive) {
                    tran = filteredPickTrans.remove(at: indexPath.row)
                }
                else {
                    tran = pickTransList.remove(at: indexPath.row)
                }
                DeletePickServer(tran)
                break
            case .putAway:
                let tran: PutPickTran
                if (self.resultSearchController.isActive) {
                    tran = filteredPutTrans.remove(at: indexPath.row)
                }
                else {
                    tran = putTransList.remove(at: indexPath.row)
                }
                DeletePutAwayServer(tran)
                break
            default:
                assert(false, "Invalid screen type")
                return
            }
            
            self.transTable.reloadData()
        }
    }
    
    func DeleteAuditStockServer(_ tran: auditStockData) {
        baseDeleteDataOnServer(tran, nil)
    }
    
    func DeletePutAwayServer(_ tran: PutPickTran) {
        baseDeleteDataOnServer(nil, tran)
    }
    
    func DeletePickServer(_ tran: PutPickTran) {
        baseDeleteDataOnServer(nil, tran)
    }
    
    func baseDeleteDataOnServer(_ a: auditStockData?, _ p: PutPickTran?) {
        func getServerErrMsg(_ m: String) -> String {
            return String(format:TxtLocale.serverResponseFailed, m)
        }
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        self.indicator.isHidden = false
        self.indicator.startAnimating()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        var deleteTo: String
        var dateColumn: String
        var lotColumn: String
        var apiString: String
        var quantity: String
        var docNo: String = ""
        var itemNo: String = ""
        var paramString: String
        
        switch self.screenType {
        case .auditStock:
            apiString = postAuditStock
            deleteTo = "AuditStockDel"
            dateColumn = "PhysDate"
            lotColumn = "Lot"
            quantity = "Quantity"
            let tran = a! as TranData
            paramString = "Param={\"SaveTo\":\"\(deleteTo)\",\"data\":[{\"\(dateColumn)\":\"\(tran.PhysDate.toString(true))\",\"ShelfNo\": \"\(tran.ShelfNo)\",\"\(lotColumn)\":\"\(tran.Lot)\",\"BinCode\":\"\(tran.BinCode)\",\"\(quantity)\":\(tran.Quantity),\"UserID\":\"\(tran.UserID)\",\"ItemNo\": \"\(tran.ItemNo)\"}]}"
            break
        case .pick:
            apiString = postPick
            deleteTo = "PickDel"
            dateColumn = "ConfirmDate"
            lotColumn = "LotNo"
            quantity = "ConfirmedQuantity"
            docNo = p!.DocNo
            itemNo = p!.ItemNo
            let tran = p! as TranData
            paramString = "Param={\"SaveTo\":\"\(deleteTo)\",\"data\":{\"DocLineNo\":\(tran.DocLineNo.isEmpty ? "0" : tran.DocLineNo),\"DocNo\":\"\(docNo)\",\"ItemNo\": \"\(itemNo)\",\"\(lotColumn)\":\"\(tran.Lot)\",\"BinCode\":\"\(tran.BinCode)\",\"\(quantity)\":\(tran.Quantity),\"UserID\":\"\(tran.UserID)\"}}"
            break
        case .putAway:
            apiString = postPutAway
            deleteTo = "PutAwayDel"
            dateColumn = "ConfirmDate"
            lotColumn = "LotNo"
            quantity = "ConfirmedQuantity"
            docNo = p!.DocNo
            itemNo = p!.ItemNo
            let tran = p! as TranData
            paramString = "Param={\"SaveTo\":\"\(deleteTo)\",\"data\":{\"DocLineNo\":\(tran.DocLineNo.isEmpty ? "0" : tran.DocLineNo),\"DocNo\":\"\(docNo)\",\"ItemNo\": \"\(itemNo)\",\"\(lotColumn)\":\"\(tran.Lot)\",\"BinCode\":\"\(tran.BinCode)\",\"\(quantity)\":\(tran.Quantity),\"UserID\":\"\(tran.UserID)\"}}"
            break
        default:
            assert(false, "Invalid screen type")
            return
        }
        
        let postData = NSMutableData(data: paramString.data(using: String.Encoding.utf8)!)
        
        let rest = RestManager(host: apiHost)
        let result = rest.post(apiString, postJsonData: postData) { (data, response, error) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            guard error == nil else {
                self.processLoadTransResult(false, message: String(format: TxtLocale.serverNotReach, apiString))
                print(error!)
                return
            }
            
            guard let responseData = data else {
                self.processLoadTransResult(false, message: getServerErrMsg("Error: did not receive data"))
                return
            }
            
            // parse the result as JSON, since that's what the API provides
            do {
                guard let resonseDict = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: Any]
                    else {
                        self.processLoadTransResult(false, message: getServerErrMsg("Could not get JSON from responseData as dictionary"))
                        return
                }
                guard (resonseDict["message"] as? String) == nil else {
                    self.processLoadTransResult(false, message: resonseDict["message"] as! String)
                    return
                }
                guard let success = resonseDict["Success"] as? Bool else {
                    self.processLoadTransResult(false, message: getServerErrMsg("Could not parse response data1"))
                    return
                }
                
                var msg = ""
                if (!success) {
                    guard let resData = resonseDict["ResponseData"] as? [String: AnyObject] else {
                        self.processLoadTransResult(false, message: getServerErrMsg("Could not parse response data2"))
                        return
                    }
                    
                    guard let messages = resData["Message"] as? [String: AnyObject] else {
                        self.processLoadTransResult(false, message: getServerErrMsg("Could not parse response data3"))
                        return
                    }
                    
                    guard let message = messages[TxtLocale.symbol] as? String else {
                        self.processLoadTransResult(false, message: getServerErrMsg("Could not parse response data4"))
                        return
                    }
                    msg = message
                }
                
                switch self.screenType {
                case .auditStock:
                    self.auditTransList.removeAll(keepingCapacity: true)
                    break
                case .pick:
                    self.pickTransList.removeAll(keepingCapacity: true)
                    break
                case .putAway:
                    self.putTransList.removeAll(keepingCapacity: true)
                    break
                default:
                    assert(false, "Invalid screen type")
                    return
                }
                
                self.loadTransDataFromServer()
                self.processLoadTransResult(success, message: msg)
                
            } catch  {
                self.processLoadTransResult(false, message: String(format: TxtLocale.serverNotReach, apiString))
                return
            }
        }
        
        if (!result.success) {
            processLoadTransResult(false, message: result.msg)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.screenType {
        case .auditStock:
            if (self.resultSearchController.isActive) {
                return self.filteredAuditTrans.count
            }
            else {
                return self.auditTransList.count
            }
        case .pick:
            if (self.resultSearchController.isActive) {
                return self.filteredPickTrans.count
            }
            else {
                return self.pickTransList.count
            }
        case .putAway:
            if (self.resultSearchController.isActive) {
                return self.filteredPutTrans.count
            }
            else {
                return self.putTransList.count
            }
        default:
            assert(false, "Invalid screen type")
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = transTable.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath) as! TransactionCell
        
        let tran: TranData
        switch self.screenType {
        case .auditStock:
            if (self.resultSearchController.isActive) {
                if indexPath.row < filteredAuditTrans.count {
                    tran = filteredAuditTrans[indexPath.row]
                } else {
                    return TransactionCell()
                }
            }
            else {
                if indexPath.row < auditTransList.count {
                    tran = auditTransList[indexPath.row]
                } else {
                    return TransactionCell()
                }
            }
            break
        case .pick:
            if (self.resultSearchController.isActive) {
                if indexPath.row < filteredPickTrans.count {
                    tran =  filteredPickTrans[indexPath.row]
                } else {
                    return TransactionCell()
                }
            }
            else {
                if indexPath.row < pickTransList.count {
                    tran = pickTransList[indexPath.row]
                } else {
                    return TransactionCell()
                }
            }
            break
        case .putAway:
            if (self.resultSearchController.isActive) {
                if indexPath.row < filteredPutTrans.count {
                    tran = filteredPutTrans[indexPath.row]
                } else {
                    return TransactionCell()
                }
            }
            else {
                if indexPath.row < putTransList.count {
                    tran = putTransList[indexPath.row]
                } else {
                    return TransactionCell()
                }
            }
            break
        default:
            assert(false, "Invalid screen type")
            return TransactionCell()
        }
        
        cell.lotText?.text = String(format: TxtLocale.cellLot, tran.Lot)
        cell.binText?.text = String(format: TxtLocale.cellBin, tran.BinCode)
        cell.dateText?.text = tran.PhysDate.toDisplayString(datePart: true, timePart: false)
        cell.timeText?.text = tran.PhysDate.toDisplayString(datePart: false, timePart: true)
        cell.amountText?.text = tran.Quantity.toCurrecyDigits(numOfPlace: 2)
        
        if (tran.isEntry) {
            cell.backgroundColor = UIColor.yellow
        } else {
            cell.backgroundColor = UIColor.white
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //filesTable.deselectRow(at: indexPath, animated: true)
        
    }
    
    static func checkExistItemAudit(_ ss: String, _ data: auditStockData) -> Bool
    {
        return checkExistItem(ss, data) || (data.PhysDateText.contains(ss) || data.amountText.contains(ss))
    }
    
    static func checkExistItemPutPick(_ ss: String, _ data: PutPickTran) -> Bool
    {
        return checkExistItem(ss, data) || (data.DocNo.contains(ss) || data.createDateText.contains(ss) || data.amountText.contains(ss))
    }

    static func checkExistItem(_ ss: String, _ data: TranData) -> Bool
    {
        return (data.ItemNo.contains(ss) || data.BinCode.contains(ss) || data.Lot.contains(ss) || data.DocLineNo.contains(ss))
    }
    
    func updateSearchResults(for searchController: UISearchController)
    {
        let ss = searchController.searchBar.text!
        let ssEmpty = ss.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty
        
        switch self.screenType {
        case .auditStock:
            filteredAuditTrans.removeAll(keepingCapacity: false)
            
            if (ssEmpty) {
                filteredAuditTrans = auditTransList
            } else {
                filteredAuditTrans = auditTransList.filter({ (data) -> Bool in
                    return ReportTransViewController.checkExistItemAudit(ss, data as auditStockData)
                })
            }
            break
        case .pick:
            filteredPickTrans.removeAll(keepingCapacity: false)
            
            if (ssEmpty) {
                filteredPickTrans = pickTransList
            } else {
                filteredPickTrans = pickTransList.filter({ (data) -> Bool in
                    return ReportTransViewController.checkExistItemPutPick(ss, data as PutPickTran)
                })
            }
            break
        case .putAway:
            filteredPutTrans.removeAll(keepingCapacity: false)
            
            if (ssEmpty) {
                filteredPutTrans = putTransList
            } else {
                filteredPutTrans = putTransList.filter({ (data) -> Bool in
                    return ReportTransViewController.checkExistItemPutPick(ss, data as PutPickTran)
                })
            }
            break
        default:
            assert(false, "Invalid screen type")
            return
        }
        
        DispatchQueue.main.async {
            self.transTable.reloadData()
        }
    }
}
