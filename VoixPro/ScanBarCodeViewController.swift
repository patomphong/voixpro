//
//  ScanBarCodeViewController.swift
//  VoixPro
//
//  Created by Patomphong Wongkalasin on 12/14/2560 BE.
//  Copyright © 2560 Song. All rights reserved.
//

import UIKit
import AVFoundation
import SwiftQRCode

class ScanBarCodeViewController: UIViewController {

    let scanner = QRCode()
    var screenType: screenType = .none
    var scanBarcodeType: scanBarcodeType = .none
    var scanBarcodeResult: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        scanner.prepareScan(view) { (scanResult) -> () in
            if scanResult != "" {
                print("result : " + scanResult)
                self.scanBarcodeResult = scanResult
                self.performSegue(withIdentifier: "unwindToAddTransactionVC", sender: self)
            }
        }
        // scan frame
        scanner.scanFrame = view.bounds
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // start scan
        scanner.startScan()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        // stop scan
        scanner.stopScan()
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "unwindToAddTransactionVC" {
            let controller = segue.destination as! AddTransactionViewController
            controller.screenType = self.screenType
            controller.scanBarcodeType = self.scanBarcodeType
            controller.scanBarcodeResult = self.scanBarcodeResult
        }
    }
}
