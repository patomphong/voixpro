//
//  IViewController.swift
//  VoixPRO
//
//  Created by Song on 12/19/2559 BE.
//  Copyright © 2559 Song. All rights reserved.
//

import Foundation

protocol LocaleSettingDelegate {
    func setScreenDisplayAndControlsLocale(_ isSet: Bool)
}
