//
//  TransactionData.swift
//  VoixPRO
//
//  Created by Song on 11/27/2559 BE.
//  Copyright © 2559 Song. All rights reserved.
//

import Foundation

protocol TranData {
    var PhysDate: Date {get set}
    var Lot: String {get set}
    var BinCode: String {get set}
    var Quantity: Double {get set}
    var ShelfNo: String {get set}
    var UserID: String {get set}
    
    var isEntry: Bool {get set}
    var DocLineNo: String {get set}
    var ItemNo: String {get set}
}

protocol PutPickData : TranData {
    var DocNo: String {get set}
    var OrderNo: String {get set}
    var ItemName: String {get set}
    var CreatedDate: Date? {get set}
    
}

class auditStockData : TranData {
    var PhysDate: Date = Date()
    var Lot: String = ""
    var BinCode: String = ""
    var Quantity: Double = 0
    var ShelfNo: String = ""
    var UserID: String = ""
    
    var isEntry: Bool = false
    var DocLineNo: String = ""
    var rowId = 0
    var ItemNo: String = ""
    var PhysDateText: String = ""
    var amountText: String = ""

}

class PutPickTran : PutPickData {
    var PhysDate: Date = Date()
    var Lot: String = ""
    var BinCode: String = ""
    var Quantity: Double = 0
    var ShelfNo: String = ""
    var UserID: String = ""
    var isEntry: Bool = false
    
    var DocNo: String = ""
    var OrderNo: String = ""
    var ItemNo: String = ""
    var ItemName: String = ""
    var CreatedDate: Date? = nil
    var DocLineNo: String = ""
    
    var createDateText: String = ""
    var amountText: String = ""

}
