//
//  MainNavigationViewContoller.swift
//  VoixPRO
//
//  Created by Song on 12/1/2559 BE.
//  Copyright © 2559 Song. All rights reserved.
//

import Foundation
import UIKit

class MainNavigationViewContoller : UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Status bar white font
        //self.navigationBar.barStyle = UIBarStyle.Black
        self.navigationBar.tintColor = UIColor.white
    }
}
