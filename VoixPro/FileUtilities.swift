//
//  FileUtilities.swift
//  VoixPRO
//
//  Created by Song on 12/2/2559 BE.
//  Copyright © 2559 Song. All rights reserved.
//

import Foundation
import UIKit

class fileUtilties {
    
    class func pathToFile(_ fileName:String, ext:String)->String {
        let documentsPath =  try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        var destinationPath = documentsPath.relativePath
        destinationPath.append("/\(fileName).\(ext)")
        
        if(!FileManager.default.fileExists(atPath: destinationPath as String) ){
            
            let fileForCopy = Bundle.main.path(forResource: fileName,ofType:ext)
            do{
                try FileManager.default.copyItem(atPath: fileForCopy!, toPath: destinationPath as String)
            } catch {
                UIAlertView(title: "error", message: "copy file to local error", delegate: nil, cancelButtonTitle:"cancel").show()
            }
            
            return destinationPath as String
        }
        else{
            return destinationPath as String
        }
    }
    
    class func SaveDataInputToFile(_ content: String, _ inputFilePath: String) {
        let txtPath = pathToFile(inputFilePath, ext: "txt")
        let contentToAppend = content + "\n"
        
        //Check if file exists
        if let fileHandle = FileHandle(forWritingAtPath: txtPath) {
            //Append to file
            fileHandle.seekToEndOfFile()
            fileHandle.write(contentToAppend.data(using: String.Encoding.utf8)!)
        }
        else {
            //Create new file
            do {
                try contentToAppend.write(toFile: txtPath, atomically: true, encoding: String.Encoding.utf8)
            } catch {
                print("Error creating \(txtPath)")
            }
        }
    }
    
    class func GetApiSettingFromConfigFile() {
        let txtPath = pathToFile("config", ext: "txt")
        let configPath = URL(fileURLWithPath: txtPath, isDirectory: true)
        
        //reading
        do {
            let text2 = try String(contentsOf: configPath, encoding: String.Encoding.utf8)
            let newLineChars = CharacterSet.newlines // newline characters defined as (U+000A–U+000D, U+0085)
            let configsRaw = text2.components(separatedBy: newLineChars)
            let configsSplit = configsRaw.filter({ (d) -> Bool in
                return !d.isEmpty
            })
            
            if (configsSplit.count < 10) {
                SaveApiSettingsToConfigFile()
                return
            }
            
            apiHost = configsSplit[0]
            postLogin = configsSplit[1]
            postAuditStock = configsSplit[2]
            queryAuditStock = configsSplit[3]
            postPutAway = configsSplit[4]
            queryPutAwayOrder = configsSplit[5]
            queryPutAway = configsSplit[6]
            postPick = configsSplit[7]
            queryPickOrder = configsSplit[8]
            queryPick = configsSplit[9]
        }
        catch {/* error handling here */}
    }
    
    class func SaveApiSettingsToConfigFile() {
        let txtPath = pathToFile("config", ext: "txt")
        let configPath = URL(fileURLWithPath: txtPath, isDirectory: true)
        //writing
        do {
            let allText = "\(apiHost)\r\n\(postLogin)\r\n\(postAuditStock)\r\n\(queryAuditStock)\r\n\(postPutAway)\r\n\(queryPutAwayOrder)\r\n\(queryPutAway)\r\n\(postPick)\r\n\(queryPickOrder)\r\n\(queryPick)\r\n"
            try allText.write(to: configPath, atomically: false, encoding: String.Encoding.utf8)
        }
        catch {/* error handling here */}
        
    }
}
