//
//  MainMenuViewController.swift
//  VoixPRO
//
//  Created by Song on 10/31/2559 BE.
//  Copyright © 2559 Song. All rights reserved.
//


import UIKit

class MainMenuViewController : UIViewController, LocaleSettingDelegate {
    
    @IBOutlet var auditStockBtn: UIButton!
    @IBOutlet var putAwayBtn: UIButton!
    @IBOutlet var pickBtn: UIButton!
    @IBOutlet var reportBtn: UIButton!
    @IBOutlet var nameOfUserText: UILabel!
    @IBOutlet var welcomeText: UILabel!
    @IBOutlet var lotoutButton: UIButton!
    @IBOutlet var langButton: UIButton!
    
    let showAduitSegueID = "showAuditStock"
    let showPickSegueID = "showPick"
    let showPutAwaySegueID = "showPutAway"
    let showReportSegueID = "showReport"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setScreenDisplayAndControlsLocale()
        
        nameOfUserText.text = UserInfos.getUserFullName()
        
        disableButtonWithoutShelfNo()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func languageTouchup(_ sender: AnyObject) {
        setScreenDisplayAndControlsLocale(true)
    }
    
    // Set screen display language
    func setScreenDisplayAndControlsLocale(_ isSet: Bool = false) {
        Utilities.setScreenDisplayLocale(isSet, langButton, setControlsDisplayTitle: setControlsDisplayTitle)
    }
    
    // Set title text to button
    func setControlsDisplayTitle(_ l: TxtLocaleAbs) {
        self.navigationItem.title = l.mainMenuTitle
        auditStockBtn.setTitle(l.auditButtonTitle, for: UIControlState.normal)
        putAwayBtn.setTitle(l.putAwayButtontitle, for: UIControlState.normal)
        pickBtn.setTitle(l.pickButtonTitle, for: UIControlState.normal)
        reportBtn.setTitle(l.reportButtonTitle, for: UIControlState.normal)
        lotoutButton.setTitle(l.logoutButtonTitle, for: UIControlState.normal)
        welcomeText.text = l.welcomeTitle
    }
    
    //New
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let sid = segue.identifier else {
            super.prepare(for: segue, sender: sender)
            return
        }
        
        // Switch destination by given sender id
        switch sid {
        case showAduitSegueID:
            if let destination = segue.destination as? AddTransactionViewController {
                destination.screenType = .auditStock
                destination.localeFuncParent = self
            }
            break
        case showPickSegueID:
            if let destination = segue.destination as? PutAwayViewController {
                destination.screenType = .pick
                destination.localeFuncParent = self
            }
            break
        case showPutAwaySegueID:
            if let destination = segue.destination as? PutAwayViewController {
                destination.screenType = .putAway
                destination.localeFuncParent = self
            }
            break
        case showReportSegueID:
            if let destination = segue.destination as? MainReportViewController {
                destination.localeFuncParent = self
            }
            break
        default:
            break
        }
        
        super.prepare(for: segue, sender: sender)
    }
    
    @IBAction func unwindMainMenu(segue: UIStoryboardSegue) {
        setScreenDisplayAndControlsLocale()
    }
    
    func disableButtonWithoutShelfNo() {
        if (UserInfos.shelfNo == nil || UserInfos.shelfNo!.isEmpty) {
            putAwayBtn.isEnabled = false
            pickBtn.isEnabled = false
        }
    }
}
