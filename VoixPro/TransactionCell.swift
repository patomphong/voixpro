//
//  BrowseFileCustomCell.swift
//  PrintAnywhere
//
//  Created by Song on 10/11/2559 BE.
//  Copyright © 2559 One2Print. All rights reserved.
//

import Foundation
import UIKit

class TransactionCell: UITableViewCell {
    @IBOutlet weak var leftImage: UIImageView!
    @IBOutlet weak var lotText: UILabel!
    @IBOutlet weak var binText: UILabel!
    @IBOutlet weak var amountText: UILabel!
    @IBOutlet weak var dateText: UILabel!
    @IBOutlet weak var timeText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
