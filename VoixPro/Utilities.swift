//
//  Utilities.swift
//  PrintAnywhere
//
//  Created by Song on 10/14/2559 BE.
//  Copyright © 2559 One2Print. All rights reserved.
//

import UIKit

/// All transaction screen type
///
/// - none: not set
/// - auditStock: auditStock screen
/// - putAway: putAway screen
/// - pick: pick screen
enum screenType {
    case none
    case auditStock
    case putAway
    case pick
}

enum scanBarcodeType {
    case none
    case item
    case bin
}

/// Utilities class
class Utilities {
    /// Set screen language display text
    ///
    /// - Parameters:
    ///   - isSet: also toggle language
    ///   - langButton: langButton control
    ///   - setControlsDisplayTitle: function call after finished set
    class func setScreenDisplayLocale(_ isSet: Bool, _ langButton: UIButton, setControlsDisplayTitle: @escaping (_ l: TxtLocaleAbs) -> Swift.Void) {
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        switch UserInfos.lang {
        case .english:
            if isSet {
                UserInfos.lang = .thai
                langButton.setImage(UIImage(named: "icon_en"), for: .normal)
                TxtLocale = DispTH
            } else {
                langButton.setImage(UIImage(named: "icon_th"), for: .normal)
                TxtLocale = DispEn
            }
            break
        case .thai:
            if isSet {
                UserInfos.lang = .english
                langButton.setImage(UIImage(named: "icon_th"), for: .normal)
                TxtLocale = DispEn
            } else {
                langButton.setImage(UIImage(named: "icon_en"), for: .normal)
                TxtLocale = DispTH
            }
            break
        }
        
        setControlsDisplayTitle(TxtLocale)
        
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    /// Resize image
    ///
    /// - Parameters:
    ///   - image: image control
    ///   - targetSize: new size
    /// - Returns: new image control
    class func ResizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width:size.width * heightRatio, height:size.height * heightRatio)
        } else {
            newSize = CGSize(width:size.width * widthRatio, height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x:0, y:0, width: newSize.width, height:newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    /// Create alert dialog
    ///
    /// - Parameters:
    ///   - ttl: title
    ///   - msg: message
    ///   - cancelTitle: cancel Title
    /// - Returns: new alert controller
    class func makeAlert(_ ttl:String,msg:String,cancelTitle:String,handler: ((UIAlertAction) -> Void)? = nil) -> UIAlertController {
        let title = NSLocalizedString(ttl, comment: "")
        let message = NSLocalizedString(msg, comment: "")
        let cancelButtonTitle = NSLocalizedString(cancelTitle, comment: "")
        
        let alertCotroller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        // Create the actions.
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .cancel, handler: handler)
        // Add the actions.
        alertCotroller.addAction(cancelAction)
        
        return alertCotroller
    }
}
