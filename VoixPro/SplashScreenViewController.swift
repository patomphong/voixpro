//
//  SplashScreenViewController.swift
//  VoixPRO
//
//  Created by Song on 11/5/2559 BE.
//  Copyright © 2559 Song. All rights reserved.
//

import UIKit

class SplashScreenViewController: UIViewController {
    var imgView: UIImageView!
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let triggerTime = DispatchTime.now() + Double((0.9))
        DispatchQueue.main.asyncAfter(deadline: (triggerTime), execute: { () -> Void in
            self.performSegue(withIdentifier: "RedirectMainSegue", sender: self)
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}
