//
//  PutAwayViewController.swift
//  VoixPRO
//
//  Created by Song on 11/28/2559 BE.
//  Copyright © 2559 Song. All rights reserved.
//

import UIKit

/// Put away & Pick view controller for select a traction to enter
class PutAwayViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, LocaleSettingDelegate {
    
    /// UI Control binding
    @IBOutlet var transTable: UITableView!
    @IBOutlet var indicator: UIActivityIndicatorView!
    @IBOutlet var langButton: UIButton!
    
    // Current screen showing
    var screenType: screenType = .none
    var queryForScreenType: String = ""
    
    // Flag to check do a reload
    static var isNeedReload = false
    
    /// set constanst string id resusable table view cell
    let textCellIdentifier = "TextCell"
    /// set constanst string id for each segue action
    let showTransactionSegueIdentifier = "showAddTransaction"
    
    /// List of display transaction
    var transList: [PutPickTran] = []
    
    /// List of filtered display data
    var filteredTableData: [PutPickTran] = []
    /// UI Search controller
    var resultSearchController = UISearchController()
    
    /// This language display setting function used with sub view controller to call out
    var localeFuncParent: LocaleSettingDelegate?
    
    /// ON view move to other view
    ///
    /// - Parameter parent: parent view
    override func willMove(toParentViewController parent: UIViewController?) {
        super.willMove(toParentViewController: parent)
        if parent == nil {
            // The back button was pressed or interactive gesture used
            if let del = localeFuncParent {
                del.setScreenDisplayAndControlsLocale(false)
            }
        }
    }
    
    /// Load data from web service
    func performLoadView() {
        
        DispatchQueue.main.async {
            UIApplication.shared.beginIgnoringInteractionEvents()
            self.indicator.isHidden = false
            self.indicator.startAnimating()
            
            self.transList = []
            self.loadTransDataFromServer()
        }
    }
    
    /// Load view
    override func viewDidLoad() {
        // Adjust inset of table
        transTable.separatorInset.right = transTable.separatorInset.left;
        
        super.viewDidLoad()
        
        setScreenDisplayAndControlsLocale()
        
        // Hide indicator
        indicator.isHidden = true
        
        // Create a search controller
        self.resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            
            transTable.tableHeaderView = controller.searchBar
            
            return controller
        })()
        
        
        // Do any additional setup after loading the view, typically from a nib.
        transTable.delegate = self
        transTable.dataSource = self
        
        // Perform load data
        performLoadView()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func languageTouchup(_ sender: AnyObject) {
        setScreenDisplayAndControlsLocale(true)
    }
    
    func setScreenDisplayAndControlsLocale(_ isSet: Bool = false) {
        Utilities.setScreenDisplayLocale(isSet, langButton, setControlsDisplayTitle: setControlsDisplayTitle)
        if PutAwayViewController.isNeedReload {
            performLoadView()
            PutAwayViewController.isNeedReload = false
        }
    }
    
    func setControlsDisplayTitle(_ l: TxtLocaleAbs) {
        switch screenType {
        case .pick:
            self.navigationItem.title = TxtLocale.pickTitle
            queryForScreenType = queryPickOrder
            break
        case .putAway:
            self.navigationItem.title = TxtLocale.putAwayTitle
            queryForScreenType = queryPutAwayOrder
            break
        default:
            assert(false, "Invalid screen type")
            break
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        //self.resultSearchController.setEditing(false, animated: false)
        
        super.viewWillDisappear(animated)
    }
    
    //New
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.showTransactionSegueIdentifier {
            if let destination = segue.destination as? AddTransactionViewController {
                if let trnIndex = (self.transTable.indexPathForSelectedRow as NSIndexPath?)?.row {
                    let data = (self.resultSearchController.isActive ? self.filteredTableData : self.transList)[trnIndex]
                    
                    // Deactivate search bar
                    self.resultSearchController.isActive = false;
                    
                    destination.originalSelectedData = data
                    destination.selectedData = PutPickTran()
                    destination.selectedData!.Lot = ""
                    destination.selectedData!.BinCode = ""
                    destination.selectedData!.Quantity = 0
                    destination.screenType = self.screenType
                    
                    destination.localeFuncParent = self
                    
                }
                return;
            }
        }
        
        super.prepare(for: segue, sender: sender)
    }
    
    /// Load data from web service
    func loadTransDataFromServer() {
        /// Get error message from web service
        ///
        /// - Parameter m: description
        /// - Returns: formatted string
        func getServerErrMsg(_ m: String) -> String {
            return String(format:TxtLocale.serverResponseFailed, m)
        }
        
        /// Create Json data parameter string
        let paramString = "Param={\"ShelfNo\":\"\(UserInfos.shelfNo!)\"}"
        /// Create post data
        let postData = NSMutableData(data: paramString.data(using: String.Encoding.utf8)!)
        
        // Set network activity on.
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        /// Create a rest manager for the web service
        let rest = RestManager(host: apiHost)
        /// post data to web service
        let result = rest.post(queryForScreenType, postJsonData: postData) { (data, response, error) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            // Try check has error
            guard error == nil else {
                self.processLoadTransResult(false, message: String(format: TxtLocale.serverNotReach, self.queryForScreenType))
                print(error!)
                return
            }
            
            // Try get response data
            guard let responseData = data else {
                self.processLoadTransResult(false, message: getServerErrMsg("Error: did not receive data"))
                return
            }
            print("Response: \(response)")
            let strData = String(data: data!, encoding: String.Encoding.utf8)
            print("Body: \(strData)")
            
            // parse the result as JSON, since that's what the API provides
            do {
                // Try get as dictionary
                guard let resonseDict = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: Any]
                    else {
                        self.processLoadTransResult(false, message: getServerErrMsg("Could not get JSON from responseData as dictionary"))
                        return
                }
                
                /// Try get message in dictionary
                guard (resonseDict["message"] as? String) == nil else {
                    self.processLoadTransResult(false, message: resonseDict["message"] as! String)
                    return
                }
                
                // Try get success status in dictionary
                guard let success = resonseDict["Success"] as? Bool else {
                    self.processLoadTransResult(false, message: getServerErrMsg("Could not parse response data1"))
                    return
                }
                
                // Check success status
                if (success) {
                    /// Try get data in dictionary
                    guard let dataArr = resonseDict["Data"] as? [Any] else {
                        self.processLoadTransResult(false, message: getServerErrMsg("Could not parse response data2"))
                        return
                    }
                    
                    // Get data in array for construct transction object
                    for auditData in dataArr {
                        guard let dataDic = auditData as? [String: Any] else {
                            self.processLoadTransResult(false, message: getServerErrMsg("Could not parse response data3"))
                            return
                        }
                        guard let docNo = dataDic["DocNo"] as? String else {
                            self.processLoadTransResult(false, message: getServerErrMsg("Could not parse response data4"))
                            return
                        }
                        guard let orderNo = dataDic["RelocationOrderNo"] as? String else {
                            self.processLoadTransResult(false, message: getServerErrMsg("Could not parse response data5"))
                            return
                        }
                        guard let itemNo = dataDic["ItemNo"] as? String else {
                            self.processLoadTransResult(false, message: getServerErrMsg("Could not parse response data6"))
                            return
                        }
                        guard let itemName = dataDic["Description"] as? String else {
                            self.processLoadTransResult(false, message: getServerErrMsg("Could not parse response data7"))
                            return
                        }
                        guard let lot = dataDic["LotNo"] as? String else {
                            self.processLoadTransResult(false, message: getServerErrMsg("Could not parse response data8"))
                            return
                        }
                        guard let binCode = dataDic["BinCode"] as? String else {
                            self.processLoadTransResult(false, message: getServerErrMsg("Could not parse response data9"))
                            return
                        }
                        guard let createDate = dataDic["CreatedDate"] as? String else {
                            self.processLoadTransResult(false, message: getServerErrMsg("Could not parse response data10"))
                            return
                        }
                        guard let quantity = dataDic["Quantity"] as? Double else {
                            self.processLoadTransResult(false, message: getServerErrMsg("Could not parse response data11"))
                            return
                        }
                        guard let docLineNo = dataDic["DocLineNo"] as? Float else {
                            self.processLoadTransResult(false, message: getServerErrMsg("Could not parse response data12"))
                            return
                        }
                        
                        // Create a transaction object with retrieved data from web service
                        let trn = PutPickTran()
                        trn.isEntry = false
                        trn.BinCode = binCode
                        trn.Lot = lot
                        trn.ItemNo = itemNo
                        trn.ItemName = itemName
                        trn.CreatedDate = createDate.toDate()
                        trn.Quantity = quantity
                        trn.ShelfNo = UserInfos.shelfNo!
                        trn.DocNo = docNo
                        trn.OrderNo = orderNo
                        trn.UserID = UserInfos.userName!
                        trn.DocLineNo = String(docLineNo)
                        trn.createDateText = trn.CreatedDate!.toDisplayString()
                        trn.amountText = trn.Quantity.toCurrecyDigits(numOfPlace: 2)
                        
                        // Push in transaction array
                        self.transList.append(trn)
                    }
                }
                else
                {
                    // Not success then clear list
                    self.transList = []
                }
                
                // Process result message
                self.processLoadTransResult(success, message: getServerErrMsg(""))
                
            } catch  {
                // Process result message
                self.processLoadTransResult(false, message: String(format: TxtLocale.serverNotReach, self.queryForScreenType))
                return
            }
        }
        
        // If not success should process result message
        if (!result.success) {
            processLoadTransResult(false, message: result.msg)
        }
    }
    
    /// Process post data result
    ///
    /// - Parameters:
    ///   - succeeded: succeeded status
    ///   - message: message
    func processLoadTransResult(_ succeeded: Bool, message: String = "") {
        //sleep(4)
        DispatchQueue.main.async {
            
            // Reload the table
            self.transTable.reloadData()
            
            // hide indicator
            self.indicator.isHidden = true
            self.indicator.stopAnimating()
            UIApplication.shared.endIgnoringInteractionEvents()
            
            if (succeeded) {
                //self.performSegue(withIdentifier: "showMainMenu", sender: self)
            }
            else
            {
                // Alert with error message
                let alertCotroller = Utilities.makeAlert("", msg: message, cancelTitle: "OK")
                self.present(alertCotroller, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: - UITextFieldDelegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Count data in filtered table if search ui is active
        if (self.resultSearchController.isActive) {
            return self.filteredTableData.count
        }
        else {
            // Otherwise count in ful transaction array
            return self.transList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        /// Dequeue reuseable cell for table view
        let cell = transTable.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath) as! PutAwayTransCell
        
        let tran: PutPickTran
        
        /// Use data in filtered array if search ui is active
        if (self.resultSearchController.isActive) {
            if indexPath.row < filteredTableData.count {
                tran = filteredTableData[indexPath.row]
            } else {
                return PutAwayTransCell()
            }
        }
        else {
            if indexPath.row < transList.count {
                tran = transList[indexPath.row]
            } else {
                return PutAwayTransCell()
            }
        }
        
        // Set cell text with selected transaction data
        cell.binShelfLabel?.text = TxtLocale.cellBinShelf
        cell.lotNoLabel?.text = TxtLocale.cellLotNo
        cell.docnoText?.text = tran.DocNo
        cell.relText?.text = tran.OrderNo
        cell.itemNoText?.text = tran.ItemNo
        cell.itemNameText?.text = tran.ItemName
        cell.binShelfText?.text = String.init(format: "%@/%@", tran.BinCode
            , tran.ShelfNo)
        cell.dateText?.text = tran.CreatedDate!.toDisplayString(datePart: true, timePart: false)
        cell.timeText?.text = tran.CreatedDate!.toDisplayString(datePart: false, timePart: true)
        cell.amountText?.text = tran.amountText
        cell.lotNoText?.text = tran.Lot
        
        // Is data from database on this device, if so then use yellow background
        if (tran.isEntry) {
            cell.backgroundColor = UIColor.yellow
        } else {
            cell.backgroundColor = UIColor.white
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //filesTable.deselectRow(at: indexPath, animated: true)
    }
    
    func updateSearchResults(for searchController: UISearchController)
    {
        // Remove old data
        filteredTableData.removeAll(keepingCapacity: false)
        
        /// current search text
        let ss = searchController.searchBar.text!
        
        // Check empty
        if (ss.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty) {
            // show full list
            filteredTableData = transList
        } else {
            // check all field
            filteredTableData = transList.filter({ (data) -> Bool in
                return (data.DocNo.contains(ss) || data.OrderNo.contains(ss) || data.ItemNo.contains(ss) || data.ItemName.contains(ss) || data.BinCode.contains(ss) || data.ShelfNo.contains(ss) || data.createDateText.contains(ss) || data.amountText.contains(ss) || data.Lot.contains(ss))
            })
        }
        
        // Reload filtered data table
        self.transTable.reloadData()
    }
}

