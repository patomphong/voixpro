//
//  UserInfos.swift
//  VoixPRO
//
//  Created by Song on 11/26/2559 BE.
//  Copyright © 2559 Song. All rights reserved.
//

import Foundation

enum DisplayLanguage {
    case english
    case thai
}

class UserInfos {
    static var lang: DisplayLanguage = .english
    static var firstName: String?
    static var lastName: String?
    static var email: String?
    static var userName: String?
    static var shelfNo: String?
    
    static func clear(){
        firstName = ""
        lastName = ""
        email = ""
        userName = ""
        shelfNo = nil
        lang = .english
    }
    
    static func getUserFullName() -> String {
        return "\(firstName!) \(lastName!)"
    }
}
