//
//  MainReportViewController.swift
//  VoixPRO
//
//  Created by Song on 10/31/2559 BE.
//  Copyright © 2559 Song. All rights reserved.
//

import UIKit

class MainReportViewController : UIViewController, LocaleSettingDelegate {

    @IBOutlet var nameOfUserText: UILabel!
    @IBOutlet var langButton: UIButton!
    @IBOutlet var auditStockBtn: UIButton!
    @IBOutlet var putAwayBtn: UIButton!
    @IBOutlet var pickBtn: UIButton!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var topLabel: UILabel!
    
    let showAduitSegueIdentifier = "showAuditStock"
    let showPickSegueIdentifier = "showPick"
    let showPutAwaySegueIdentifier = "showPutAway"
    
    var localeFuncParent: LocaleSettingDelegate?
    
    override func willMove(toParentViewController parent: UIViewController?) {
        super.willMove(toParentViewController: parent)
        if parent == nil {
            // The back button was pressed or interactive gesture used
            if let del = localeFuncParent {
                del.setScreenDisplayAndControlsLocale(false)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.setHidesBackButton(true, animated:true);
        
        nameOfUserText.text = UserInfos.getUserFullName()
        setScreenDisplayAndControlsLocale()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.navigationItem.hidesBackButton = true
    }
    
    @IBAction func languageTouchup(_ sender: AnyObject) {
        setScreenDisplayAndControlsLocale(true)
    }
    
    func setScreenDisplayAndControlsLocale(_ isSet: Bool = false) {
        Utilities.setScreenDisplayLocale(isSet, langButton, setControlsDisplayTitle: setControlsDisplayTitle)
    }
    
    func setControlsDisplayTitle(_ l: TxtLocaleAbs) {
        self.navigationItem.title = l.dailyTransTitle
        auditStockBtn.setTitle(l.auditButtonTitle, for: UIControlState.normal)
        putAwayBtn.setTitle(l.putAwayButtontitle, for: UIControlState.normal)
        pickBtn.setTitle(l.pickButtonTitle, for: UIControlState.normal)
        backButton.setTitle(l.backButtonTitle, for: UIControlState.normal)
        topLabel.text = l.mainReportTitle
    }
    
    //New
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let sid = segue.identifier else {
            super.prepare(for: segue, sender: sender)
            return
        }
        
        switch sid {
        case showAduitSegueIdentifier:
            if let destination = segue.destination as? ReportTransViewController {
                destination.screenType = .auditStock
                destination.localeFuncParent = self
            }
            break
        case showPickSegueIdentifier:
            if let destination = segue.destination as? ReportTransViewController {
                destination.screenType = .pick
                destination.localeFuncParent = self
            }
            break
        case showPutAwaySegueIdentifier:
            if let destination = segue.destination as? ReportTransViewController {
                destination.screenType = .putAway
                destination.localeFuncParent = self
            }
            break
        default:
            break
        }
        
        super.prepare(for: segue, sender: sender)
    }
    
    @IBAction func unwindMainReport(segue: UIStoryboardSegue) {
    }
}
