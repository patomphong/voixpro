//
//  PutAwayTransCell.swift
//  VoixPRO
//
//  Created by Song on 11/28/2559 BE.
//  Copyright © 2559 Song. All rights reserved.
//

import Foundation
import UIKit

class PutAwayTransCell: UITableViewCell, LocaleSettingDelegate {
    @IBOutlet weak var leftImage: UIImageView!
    @IBOutlet weak var docnoText: UILabel!
    @IBOutlet weak var relText: UILabel!
    @IBOutlet weak var itemNoText: UILabel!
    @IBOutlet weak var itemNameText: UILabel!
    @IBOutlet weak var binShelfText: UILabel!
    @IBOutlet weak var binShelfLabel: UILabel!
    @IBOutlet weak var amountText: UILabel!
    @IBOutlet weak var dateText: UILabel!
    @IBOutlet weak var timeText: UILabel!
    @IBOutlet weak var lotNoLabel: UILabel!
    @IBOutlet weak var lotNoText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setScreenDisplayAndControlsLocale(_ isSet: Bool = false) {
        binShelfLabel.text = TxtLocale.cellBinShelf
        lotNoLabel.text = TxtLocale.cellLotNo
    }
    
}
