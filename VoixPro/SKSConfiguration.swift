//
//  SKSConfiguration.swift
//  SpeechKitSample
//
//  All Nuance Developers configuration parameters can be set here.
//
//  Copyright (c) 2015 Nuance Communications. All rights reserved.
//

import Foundation

let SKSAppKey = "41e840e6df6ac393407c448ff9939165b9a8bcf2120066084a8adbf4f47dd43565151b447fded7bf5b1c4e54afdfb96887755a9975c1a2c04b7db43ffa89745f"
let SKSAppId = "NMDPTRIAL_psboyii_gmail_com20160824101350"
let SKSServerHost = "sslsandbox.nmdp.nuancemobility.net"
let SKSServerPort = "443"

let SKSServerUrl = String(format: "nmsps://%@@%@:%@", SKSAppId, SKSServerHost, SKSServerPort)

// Only needed if using NLU/Bolt
let SKSNLUContextTag = "!NLU_CONTEXT_TAG!"

